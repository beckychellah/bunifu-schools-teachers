package io.bunifu.erp.helpers;

import android.annotation.SuppressLint;

import com.github.tibolte.agendacalendarview.models.BaseCalendarEvent;
import com.github.tibolte.agendacalendarview.models.CalendarEvent;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import io.bunifu.erp.bunifuschools.R;
import io.bunifu.erp.service.model.EventModel;
import io.bunifu.erp.utils.extras.Helpers;

public class EventsHelper {

    private Calendar cal = Calendar.getInstance();

    private Calendar startDate;
    private Calendar endDate;

    public EventsHelper(Calendar startDate, Calendar endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public static List<CalendarEvent> convertEventModels(List<EventModel> eventModels) {

        List<CalendarEvent> events = new ArrayList<>();

        Collections.sort(eventModels);

        for (EventModel model : eventModels) {
            //String title, String description, String location, int color, calendar startDate, calendar endTime, boolean allDa
            CalendarEvent event = new BaseCalendarEvent(
                    model.getTitle(),
                    model.getDescription(),
                    model.getVenue(),
                    R.color.teal_500,
                    Helpers.dateToCalendar(model.getEndDate()),
                    Helpers.dateToCalendar(model.getEndDate()),
                    false
            );

            event.setId(model.getId());

            events.add(event);
        }

        /*WeekViewEvent weekViewEvent = new WeekViewEvent(model.getId(),
                model.getTitle(),
                Helpers.dateToCalendar(model.getStartDate()),
                Helpers.dateToCalendar(model.getEndDate()));
        if (startDate.get(calendar.MONTH) == month && startDate.get(calendar.YEAR) == year)
            events.add(weekViewEvent);*/


        return events;
    }

    public String getFormattedTime() {

        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

        return sdf.format(startDate.getTime()) + "hrs - " + sdf.format(endDate.getTime()) + "hrs";
    }

    public String getFormattedDate() {

        //check if the current year = event's year
        if (startDate.get(Calendar.DAY_OF_YEAR) != cal.get(Calendar.DAY_OF_YEAR)) {
            return checkMonthsYear() + ", " + startDate.get(Calendar.YEAR);
        } else {
            return checkMonthsYear();
        }

    }

    /**
     * check if the months are the same
     *
     * @return String
     */
    private String checkMonthsYear() {

        if (startDate.get(Calendar.MONTH) != endDate.get(Calendar.MONTH))
            return getNotSameMonth();
        else
            return getSameMonth();

    }

    /**
     * Get the date if the months are not the same
     *
     * @return String
     */
    private String getNotSameMonth() {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
        return sdf.format(startDate.getTime()) + " - " + endDate.get(Calendar.MONTH);
    }

    private String getSameMonth() {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sdf = new SimpleDateFormat("EEE dd");
        //check if the event starts and ends on the same day
        if (startDate.get(Calendar.DAY_OF_MONTH) != endDate.get(Calendar.DAY_OF_MONTH)) {
            return sdf.format(startDate.getTime()) + " - " + sdf.format(endDate.getTime());
        } else
            return sdf.format(startDate.getTime());

    }
}

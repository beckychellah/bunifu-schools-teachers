package io.bunifu.erp.helpers;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DashHelper {

    private Calendar cal = Calendar.getInstance();

    private Calendar startDate;

    public DashHelper(Calendar dateFmt) {
        this.startDate = dateFmt;
    }

  /*  public static List<CalendarEvent> convertEventModels(List<EventModel> eventModels) {

        List<CalendarEvent> events = new ArrayList<>();

        Collections.sort(eventModels);

        for (EventModel model : eventModels) {
            //String title, String description, String location, int color, calendar startDate, calendar endTime, boolean allDa
            CalendarEvent event = new BaseCalendarEvent(
                    model.getTitle(),
                    model.getDescription(),
                    model.getVenue(),
                    R.color.teal_500,
                    Helpers.dateToCalendar(model.getEndDate()),
                    Helpers.dateToCalendar(model.getEndDate()),
                    false
            );

            event.setId(model.getId());

            events.add(event);
        }

        *//*WeekViewEvent weekViewEvent = new WeekViewEvent(model.getId(),
                model.getTitle(),
                Helpers.dateToCalendar(model.getStartDate()),
                Helpers.dateToCalendar(model.getEndDate()));
        if (startDate.get(calendar.MONTH) == month && startDate.get(calendar.YEAR) == year)
            events.add(weekViewEvent);*//*


        return events;
    }*/

    public String getFormattedDate() {

        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        return sdf.format(startDate.get(Calendar.DAY_OF_MONTH) + "-" + startDate.get(Calendar.MONTH) + "-" + startDate.get(Calendar.YEAR));
    }
}

   /* public String getFormattedDate() {

        //check if the current year = event's year
        if (startDate.get(calendar.DAY_OF_YEAR) != cal.get(calendar.DAY_OF_YEAR)) {
            return checkMonthsYear() + ", " + startDate.get(calendar.YEAR);
        } else {
            return checkMonthsYear();
        }

    }

    *//**
     * check if the months are the same
     *
     * @return String
     *//*
    private String checkMonthsYear() {

        if (startDate.get(calendar.MONTH) != endDate.get(calendar.MONTH))
            return getNotSameMonth();
        else
            return getSameMonth();

    }

    *//**
     * Get the date if the months are not the same
     *
     * @return String
     *//*
    private String getNotSameMonth() {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
        return sdf.format(startDate.getTime()) + " - " + endDate.get(calendar.MONTH);
    }

    private String getSameMonth() {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sdf = new SimpleDateFormat("EEE dd");
        //check if the event starts and ends on the same day
        if (startDate.get(calendar.DAY_OF_MONTH) != endDate.get(calendar.DAY_OF_MONTH)) {
            return sdf.format(startDate.getTime()) + " - " + sdf.format(endDate.getTime());
        } else
            return sdf.format(startDate.getTime());

    }
}
*/
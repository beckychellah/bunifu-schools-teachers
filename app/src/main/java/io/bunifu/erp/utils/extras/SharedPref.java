package io.bunifu.erp.utils.extras;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SharedPref {

    private SharedPreferences pref;
    private static  SharedPref sharedPref = null;

    private SharedPref(Context context) {
        pref = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static SharedPref getInstance(Context context) {
        if (sharedPref == null)
            sharedPref = new SharedPref(context);
        return sharedPref;
    }

    public String getString(String key) {
        return pref.getString(key, "");
    }

    public void putString(String key, String value) {
        pref.edit().putString(key, value).apply();
    }
}
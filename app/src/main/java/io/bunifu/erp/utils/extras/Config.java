package io.bunifu.erp.utils.extras;

/**
 * Created by kenboi on 4/25/17.
 * Constants class
 */

public class Config {

    //Test
  /*  public static final String SECRET = "VyG2HHPQqidvJnPGeu4AyVaYAr9TlsEYgGE5Ngil";
    public static final int CLIENT_ID = 1;
    public static final String BASE_URL = "http://108.61.155.236/";*/

    //prod
    public static final String SECRET = "haQMswbeyl5YPBQdmA6BGQRccjSdp5qHd07VCnVd";
    public static final int CLIENT_ID = 3;
    public static final String BASE_URL = "https://bunifu.io/";

    //pagination limit for http fetch fee statement requests
    public static final int DEFAULT_LIMIT = 50;


    public static final String OAUTH_URL = "oauth/token";
    public static final String GET_STUDENT_URL = "api/student/get/";

    //The required password length
    public static final int PASSWORD_LENGTH = 5;

    //OAUTH
    public static final String ACCESS_TOKEN = "access_token";
    public static final String REFRESH_TOKEN = "refresh_token";

    public static final String JSON_ARRAY = "json_array";

    public static final String IS_STATEMENT = "is_a_fee_statement";

    public static final String ITEMS_BUFFER = "[{\"id\":12,\"adm_no\":\"837\",\"school_id\":78,\"academic_year\":\"2016-2017\",\"term\":\"Term 1\",\"exam_type\":\"cat 1\",\"exam_date\":\"2017-10-14 13:23:28\",\"marks\":[{\"subject\":\"ENG\",\"mark\":\"65\"},{\"subject\":\"KIS\",\"mark\":\"52\"},{\"subject\":\"MAT\",\"mark\":\"78\"},{\"subject\":\"BIO\",\"mark\":\"56\"},{\"subject\":\"CHE\",\"mark\":\"78\"},{\"subject\":\"HIS\",\"mark\":\"34\"},{\"subject\":\"GEO\",\"mark\":\"21\"},{\"subject\":\"AGR\",\"mark\":\"76\"},{\"subject\":\"BUS\",\"mark\":\"45\"}],\"points\":\"15\",\"position\":\"15\",\"grade\":\"A\",\"average\":\"78\",\"total\":\"402\",\"created_at\":\"2017-05-04 13:23:28\",\"updated_at\":\"2017-05-04 13:23:28\"},{\"id\":12,\"adm_no\":\"837\",\"school_id\":78,\"academic_year\":\"2016-2017\",\"term\":\"Term 1\",\"exam_type\":\"cat 2\",\"exam_date\":\"2017-05-04 13:23:28\",\"marks\":[{\"subject\":\"ENG\",\"mark\":\"65\"},{\"subject\":\"KIS\",\"mark\":\"52\"},{\"subject\":\"MAT\",\"mark\":\"78\"},{\"subject\":\"BIO\",\"mark\":\"56\"},{\"subject\":\"CHE\",\"mark\":\"78\"},{\"subject\":\"HIS\",\"mark\":\"34\"},{\"subject\":\"GEO\",\"mark\":\"21\"},{\"subject\":\"AGR\",\"mark\":\"76\"},{\"subject\":\"BUS\",\"mark\":\"45\"}],\"points\":\"15\",\"position\":\"15\",\"grade\":\"A\",\"average\":\"78\",\"total\":\"402\",\"created_at\":\"2017-05-04 13:23:28\",\"updated_at\":\"2017-05-04 13:23:28\"},{\"id\":12,\"adm_no\":\"837\",\"school_id\":78,\"academic_year\":\"2016-2017\",\"term\":\"Term 1\",\"exam_type\":\"End Term\",\"exam_date\":\"2017-04-04 13:23:28\",\"marks\":[{\"subject\":\"ENG\",\"mark\":\"65\"},{\"subject\":\"KIS\",\"mark\":\"52\"},{\"subject\":\"MAT\",\"mark\":\"78\"},{\"subject\":\"BIO\",\"mark\":\"56\"},{\"subject\":\"CHE\",\"mark\":\"78\"},{\"subject\":\"HIS\",\"mark\":\"34\"},{\"subject\":\"GEO\",\"mark\":\"21\"},{\"subject\":\"AGR\",\"mark\":\"76\"},{\"subject\":\"BUS\",\"mark\":\"45\"}],\"points\":\"15\",\"position\":\"15\",\"grade\":\"A\",\"average\":\"78\",\"total\":\"402\",\"created_at\":\"2017-05-04 13:23:28\",\"updated_at\":\"2017-05-04 13:23:28\"},{\"id\":12,\"adm_no\":\"837\",\"school_id\":78,\"academic_year\":\"2017-2018\",\"term\":\"Term 1\",\"exam_type\":\"cat 1\",\"exam_date\":\"2017-01-23 13:23:28\",\"marks\":[{\"subject\":\"ENG\",\"mark\":\"65\"},{\"subject\":\"KIS\",\"mark\":\"52\"},{\"subject\":\"MAT\",\"mark\":\"78\"},{\"subject\":\"BIO\",\"mark\":\"56\"},{\"subject\":\"CHE\",\"mark\":\"78\"},{\"subject\":\"HIS\",\"mark\":\"34\"},{\"subject\":\"GEO\",\"mark\":\"21\"},{\"subject\":\"AGR\",\"mark\":\"76\"},{\"subject\":\"BUS\",\"mark\":\"45\"}],\"points\":\"15\",\"position\":\"15\",\"grade\":\"A\",\"average\":\"78\",\"total\":\"402\",\"created_at\":\"2017-05-04 13:23:28\",\"updated_at\":\"2017-05-04 13:23:28\"},{\"id\":12,\"adm_no\":\"837\",\"school_id\":78,\"academic_year\":\"2017-2018\",\"term\":\"Term 2\",\"exam_type\":\"cat 2\",\"exam_date\":\"2017-08-09 13:23:28\",\"marks\":[{\"subject\":\"ENG\",\"mark\":\"65\"},{\"subject\":\"KIS\",\"mark\":\"52\"},{\"subject\":\"MAT\",\"mark\":\"78\"},{\"subject\":\"BIO\",\"mark\":\"56\"},{\"subject\":\"CHE\",\"mark\":\"78\"},{\"subject\":\"HIS\",\"mark\":\"34\"},{\"subject\":\"GEO\",\"mark\":\"21\"},{\"subject\":\"AGR\",\"mark\":\"76\"},{\"subject\":\"BUS\",\"mark\":\"45\"}],\"points\":\"15\",\"position\":\"15\",\"grade\":\"A\",\"average\":\"78\",\"total\":\"402\",\"created_at\":\"2017-05-04 13:23:28\",\"updated_at\":\"2017-05-04 13:23:28\"},{\"id\":12,\"adm_no\":\"837\",\"school_id\":78,\"academic_year\":\"2017-2018\",\"term\":\"Term 1\",\"exam_type\":\"End Term\",\"exam_date\":\"2017-03-04 13:23:28\",\"marks\":[{\"subject\":\"ENG\",\"mark\":\"65\"},{\"subject\":\"KIS\",\"mark\":\"52\"},{\"subject\":\"MAT\",\"mark\":\"78\"},{\"subject\":\"BIO\",\"mark\":\"56\"},{\"subject\":\"CHE\",\"mark\":\"78\"},{\"subject\":\"HIS\",\"mark\":\"34\"},{\"subject\":\"GEO\",\"mark\":\"21\"},{\"subject\":\"AGR\",\"mark\":\"76\"},{\"subject\":\"BUS\",\"mark\":\"45\"}],\"points\":\"15\",\"position\":\"15\",\"grade\":\"A\",\"average\":\"78\",\"total\":\"402\",\"created_at\":\"2017-05-04 13:23:28\",\"updated_at\":\"2017-05-04 13:23:28\"},{\"id\":12,\"adm_no\":\"837\",\"school_id\":78,\"academic_year\":\"2018-2019\",\"term\":\"Term 3\",\"exam_type\":\"cat 1\",\"exam_date\":\"2017-07-07 13:23:28\",\"marks\":[{\"subject\":\"ENG\",\"mark\":\"65\"},{\"subject\":\"KIS\",\"mark\":\"52\"},{\"subject\":\"MAT\",\"mark\":\"78\"},{\"subject\":\"BIO\",\"mark\":\"56\"},{\"subject\":\"CHE\",\"mark\":\"78\"},{\"subject\":\"HIS\",\"mark\":\"34\"},{\"subject\":\"GEO\",\"mark\":\"21\"},{\"subject\":\"AGR\",\"mark\":\"76\"},{\"subject\":\"BUS\",\"mark\":\"45\"}],\"points\":\"15\",\"position\":\"15\",\"grade\":\"A\",\"average\":\"78\",\"total\":\"402\",\"created_at\":\"2017-05-04 13:23:28\",\"updated_at\":\"2017-05-04 13:23:28\"},{\"id\":12,\"adm_no\":\"837\",\"school_id\":78,\"academic_year\":\"2018-2019\",\"term\":\"Term 3\",\"exam_type\":\"cat 2\",\"exam_date\":\"2017-06-05 13:23:28\",\"marks\":[{\"subject\":\"ENG\",\"mark\":\"65\"},{\"subject\":\"KIS\",\"mark\":\"52\"},{\"subject\":\"MAT\",\"mark\":\"78\"},{\"subject\":\"BIO\",\"mark\":\"56\"},{\"subject\":\"CHE\",\"mark\":\"78\"},{\"subject\":\"HIS\",\"mark\":\"34\"},{\"subject\":\"GEO\",\"mark\":\"21\"},{\"subject\":\"AGR\",\"mark\":\"76\"},{\"subject\":\"BUS\",\"mark\":\"45\"}],\"points\":\"15\",\"position\":\"15\",\"grade\":\"A\",\"average\":\"78\",\"total\":\"402\",\"created_at\":\"2017-05-04 13:23:28\",\"updated_at\":\"2017-05-04 13:23:28\"},{\"id\":12,\"adm_no\":\"837\",\"school_id\":78,\"academic_year\":\"2018-2019\",\"term\":\"Term 3\",\"exam_type\":\"End Term\",\"exam_date\":\"2017-11-03 13:23:28\",\"marks\":[{\"subject\":\"ENG\",\"mark\":\"65\"},{\"subject\":\"KIS\",\"mark\":\"52\"},{\"subject\":\"MAT\",\"mark\":\"78\"},{\"subject\":\"BIO\",\"mark\":\"56\"},{\"subject\":\"CHE\",\"mark\":\"78\"},{\"subject\":\"HIS\",\"mark\":\"34\"},{\"subject\":\"GEO\",\"mark\":\"21\"},{\"subject\":\"AGR\",\"mark\":\"76\"},{\"subject\":\"BUS\",\"mark\":\"45\"}],\"points\":\"15\",\"position\":\"15\",\"grade\":\"A\",\"average\":\"78\",\"total\":\"402\",\"created_at\":\"2017-05-04 13:23:28\",\"updated_at\":\"2017-05-04 13:23:28\"}]";

    public static final String PARENT_KEY = "parent";

    public static final String STUDENT_KEY = "student";

    public static final String CURRENT_STUDENT = "current_student";
    public static final String FEE_STRUCTURE = "FEE_STRUCTURE";
}

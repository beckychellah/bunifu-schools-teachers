package io.bunifu.erp.utils.extras;

import android.annotation.SuppressLint;
import android.content.Context;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by kenboi on 6/20/17.
 * Contains helper methods
 */

public class Helpers {

    public static void doLogout(Context context) {

        SharedPref.getInstance(context).putString(Config.ACCESS_TOKEN, null);
        SharedPref.getInstance(context).putString(Config.REFRESH_TOKEN, null);
        SharedPref.getInstance(context).putString(Config.CURRENT_STUDENT, null);
    }

    public static String formatAsCurrency(double amount) {
        @SuppressLint("DefaultLocale")
        String temp = String.format("%,.2f", amount);
        return "Sh " + temp;
    }

    public static Calendar dateToCalendar(String dateTime) {

        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date;
        Calendar calendar = Calendar.getInstance();
        try {
            date = sdf.parse(dateTime);
            calendar.setTime(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return calendar;

    }

    public  static Calendar dateFormat(String date){
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date dateFormat;
        Calendar calendar = Calendar.getInstance();
        try {
            dateFormat = sdf.parse(date);
            calendar.setTime(dateFormat);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return calendar;
    }

    public static String formatNullables(String defaultValue, String value){
        if(defaultValue.contentEquals(""))
            return value;

        if(value == null||value.contains(defaultValue)||value.equalsIgnoreCase(defaultValue))
            return "-";
        else
            return value;
    }


}

package io.bunifu.erp.service.repository;


import android.arch.lifecycle.MutableLiveData;
import android.util.Pair;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class RetrofitObservable<T> {

    public MutableLiveData<Boolean> loading = new MutableLiveData<>();
    public MutableLiveData<T> response = new MutableLiveData<>();
    public MutableLiveData<Pair<String, HttpException>> error = new MutableLiveData<>();

    private int threadCt = Runtime.getRuntime().availableProcessors() + 6;
    private ExecutorService executor = Executors.newFixedThreadPool(threadCt);
   public Scheduler scheduler = Schedulers.from(executor);

    private boolean isDbMode = false;

    public RetrofitObservable(boolean isDbMode) {
        this.isDbMode = isDbMode;
    }

    public RetrofitObservable() {
    }

    //observes the observable passed as parameter
    public void observe(Observable<T> observable) {
        //sets loading to true
        loading.setValue(true);
        observable
                //do action on scheduler
                .subscribeOn(getScheduler())
                //observe the result on the main thread
                .observeOn(AndroidSchedulers.mainThread())
                //pass on success and failure call backs
                .subscribe(this::onSuccess, this::onError);

    }

    public void observe(Flowable<T> flowable) {
        loading.setValue(true);
        flowable.subscribeOn(getScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccess, this::onError);
    }

    public RetrofitObservable<T> observeFlowable(Flowable<T> flowable) {
        flowable.subscribeOn(getScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(t -> response.setValue(t), throwable -> {
                });
        return this;
    }

    private Scheduler getScheduler() {
        if (isDbMode) return scheduler;
        else return Schedulers.io();
    }

    private void onError(Throwable throwable) {
        //Handle exceptions
        if (throwable instanceof IOException) {
            //handle network error
            error.setValue(Pair.create("No internet connection.", null));
        } else if (throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            //handle HTTP error response code
            error.setValue(Pair.create(httpException.message(), httpException));
        } else {
            //handle other exceptions
            error.setValue(Pair.create("An internal error has occurred.", null));
        }
        //set loading to false
        loading.setValue(false);

    }

    private void onSuccess(T t) {
        //update response mutable live data
        response.setValue(t);
        //set loading false
        loading.setValue(false);
    }
}

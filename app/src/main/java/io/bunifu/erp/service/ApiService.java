package io.bunifu.erp.service;

import java.util.List;
import java.util.Map;

import io.bunifu.erp.service.model.ConfigModel;
import io.bunifu.erp.service.model.DeviceModel;
import io.bunifu.erp.service.model.EventModel;
import io.bunifu.erp.service.model.Exam;
import io.bunifu.erp.service.model.FeeStatement;
import io.bunifu.erp.service.model.FeeStructureModel;
import io.bunifu.erp.service.model.GuardianModel;
import io.bunifu.erp.service.model.OauthModel;
import io.bunifu.erp.service.model.SchoolModel;
import io.bunifu.erp.service.model.StudentModel;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface ApiService {

    @GET("api/v1/fee_statement/list?mobile=true")
    Flowable<List<FeeStatement>> fetchFeeStatements(@Header("student") Integer id);

    @GET("api/v1/fee_structure/list?mobile=true")
    Flowable<List<FeeStructureModel>> fetchFeeStructures(@Header("student") Integer id);

    @GET("api/v1/exam/list?mobile=true")
    Flowable<List<Exam>> fetchExams(@Header("student") Integer id);

    @GET("api/v1/student/event/list?mobile=true")
    Flowable<List<EventModel>> fetchEvents(@Header("student") Integer id);

    @FormUrlEncoded
    @POST("oauth/token")
    Observable<OauthModel> authenticateUser(@FieldMap Map<String, String> map);

    @GET("api/v1/guardian/profile")
    Observable<GuardianModel> fetchUserProfile();

    @GET("api/v1/guardian/students")
    Observable<List<StudentModel>> fetchStudents();

    @GET("api/v1/school/list")
    Observable<List<SchoolModel>> fetchSchools(@QueryMap Map<String, String> map);

    @FormUrlEncoded
    @POST("api/v1/auth/phone/check")
    Observable<ConfigModel> checkIfPhoneExists(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("api/v1/auth/email/check")
    Observable<ConfigModel> checkIfEmailExists(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("api/v1/auth/sign_up")
    Observable<ConfigModel> signUp(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("api/v1/auth/password_reset")
    Observable<ConfigModel> resetPassword(@FieldMap Map<String, String> params);

    @PUT("api/v1/guardian/profile")
    Observable<GuardianModel> updateProfile(@Body GuardianModel guardianModel);

    @POST("api/v1/device/store")
    Observable<DeviceModel> storeDevice(@Body DeviceModel deviceModel);

    @GET("api/v1/student/event/list?mobile=true")
    Maybe<EventModel> fetchEventById(@Header("student") Integer id, @Query("id") Integer eventId);

    @GET("api/v1/fee_statement/list?mobile=true")
    Maybe<FeeStatement> fetchStatementsById(@Header("student") Integer id, @Query("id") Integer statementId);

    @GET("api/v1/exam/list?mobile=true")
    Maybe<Exam> fetchExamsById(@Header("student") Integer id, @Query("id") Integer examId);


}

package io.bunifu.erp.service.model;


import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class StructureModel {

    @SerializedName("term")
    @Expose
    private String term;
    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("data")
    @Expose
    private List<KeyValue> data = null;

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public List<KeyValue> getData() {
        return data;
    }

    public void setData(List<KeyValue> data) {
        this.data = data;
    }

}


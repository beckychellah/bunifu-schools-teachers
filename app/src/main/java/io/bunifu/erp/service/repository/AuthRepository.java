package io.bunifu.erp.service.repository;

import android.app.Application;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import io.bunifu.erp.database.AppDatabase;
import io.bunifu.erp.database.DatabaseBuilder;
import io.bunifu.erp.service.ApiService;
import io.bunifu.erp.service.ServiceBuilder;
import io.bunifu.erp.service.model.ConfigModel;
import io.bunifu.erp.service.model.OauthModel;
import io.bunifu.erp.utils.extras.Config;
import io.reactivex.Observable;

public class AuthRepository {

    private final String AUTH_KEY = "AUTH_KEY";
    private ApiService apiService;
    private AppDatabase db;
    private RetrofitObservable retrofitObservable = new RetrofitObservable();


    public AuthRepository(Application app) {
        this.apiService = ServiceBuilder.getRxService(app);
        this.db = DatabaseBuilder.getInstance(app).getDatabase();
    }

    public Observable<OauthModel> authenticateUser(String emailOrPhone, String password) {

        Map<String, String> map = new HashMap<>();
        map.put("grant_type", "password");
        map.put("client_id", String.valueOf(Config.CLIENT_ID));
        map.put("client_secret", Config.SECRET);
        map.put("username", emailOrPhone);
        map.put("password", password);
        map.put("scope", "*");

        return apiService.authenticateUser(map)
                .flatMap(oauthModel -> {
                    ConfigModel configModel = new ConfigModel();
                    configModel.setKey(AUTH_KEY);
                    configModel.setValue(new Gson().toJson(oauthModel));
                    db.getConfigDao().insert(configModel);
                    return Observable.just(oauthModel);
                });

    }

     private OauthModel fetchOauthToken() {

        ConfigModel configModel =  db.getConfigDao()
                .fetchConfigSync(AUTH_KEY)
                .subscribeOn(retrofitObservable.scheduler)
                .blockingGet();

        if (configModel != null)
            return new Gson().fromJson(configModel.getValue(), OauthModel.class);
        return null;
    }

    public boolean isAuthenticated() {
        return fetchOauthToken() != null;
    }

    public Observable<ConfigModel> checkIfPhoneNoExists(String phone){

        Map<String, String> map = new HashMap<>();
        map.put("phone", phone);

        return apiService.checkIfPhoneExists(map);
    }

    public Observable<ConfigModel> checkIfEmailExists(String email){

        Map<String, String> map = new HashMap<>();
        map.put("email", email);

        return apiService.checkIfEmailExists(map);
    }

    public Observable<ConfigModel> signUp(Map<String, String> params){
        return apiService.signUp(params);
    }

    public Observable<ConfigModel> resetPassword(Map<String, String> params){
        return apiService.resetPassword(params);
    }

}

package io.bunifu.erp.service.repository;

import android.app.Application;
import android.util.Log;

import com.google.gson.Gson;

import io.bunifu.erp.database.AppDatabase;
import io.bunifu.erp.database.DatabaseBuilder;
import io.bunifu.erp.service.ApiService;
import io.bunifu.erp.service.ServiceBuilder;
import io.bunifu.erp.service.model.ConfigModel;
import io.bunifu.erp.service.model.DeviceModel;
import io.bunifu.erp.utils.extras.DeviceUuidFactory;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class DeviceRepository {
    private final String DEVICE_KEY = "DEVICE_KEY";
    private ApiService apiService;
    private AppDatabase db;
    private
    DeviceUuidFactory deviceUuidFactory;

    public DeviceRepository(Application application) {
        apiService = ServiceBuilder.getRxService(application);
        db = DatabaseBuilder.getInstance(application).getDatabase();
        deviceUuidFactory = new DeviceUuidFactory(application.getApplicationContext());
    }

    public void init(){
        DeviceModel deviceModel = fetchFromDB();
        DeviceModel deviceModel1 = deviceUuidFactory.getDevice();
        if (deviceModel == null || !deviceModel.getFcmToken().contentEquals(deviceModel1.getFcmToken())) {
            storeOnline(deviceModel1)
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .subscribe(deviceModel2 -> {
                        Log.e(getClass().getName(), "Success");
                    }, throwable -> {
                        Log.e(getClass().getName(), "Failure", throwable);
                    });

        }
    }

    public Observable<DeviceModel> storeOnline(DeviceModel deviceModel) {
        return apiService.storeDevice(deviceModel)
                //save in db
                .flatMap(deviceModel1 -> {
                    ConfigModel configModel = new ConfigModel();
                    configModel.setKey(DEVICE_KEY);
                    configModel.setValue(new Gson().toJson(deviceModel1, DeviceModel.class));
                    db.getConfigDao().insert(configModel);
                    return Observable.just(deviceModel1);
                });
    }

    public DeviceModel fetchFromDB() {
        return db.getConfigDao().fetchConfigSync(DEVICE_KEY)
                .flatMap(configModel -> {
                    DeviceModel deviceModel = new Gson().fromJson(configModel.getValue(), DeviceModel.class);
                    return Maybe.just(deviceModel);
                }).subscribeOn(Schedulers.io())
                .blockingGet();
    }


}

package io.bunifu.erp.service;

import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.bunifu.erp.database.AppDatabase;
import io.bunifu.erp.database.DatabaseBuilder;
import io.bunifu.erp.service.model.ConfigModel;
import io.bunifu.erp.service.model.OauthModel;
import io.bunifu.erp.utils.extras.Config;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceBuilder {

    private static final ServiceBuilder ourInstance = new ServiceBuilder();

    private static OkHttpClient okHttpClient;

    private ServiceBuilder() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        okHttpClient = new OkHttpClient();
        okHttpClient.newBuilder()
                .addInterceptor(interceptor);

        addHeader("Accept", "application/json");
        addHeader("Organization", "7aa91c60-b583-11e7-8d4d-337a5432eaf2");


    }

    public static ServiceBuilder getInstance() {
        return ourInstance;
    }


    public static ApiService getRxService(Application app) {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OauthModel oauthModel = null;
        AppDatabase db = DatabaseBuilder.getInstance(app).getDatabase();
        //ConfigModel configModel = DatabaseBuilder.getInstance(app).getDatabase().getConfigDao().getConfigSync("AUTH_KEY");

        ConfigModel configModel = db.getConfigDao()
                .fetchConfigSync("AUTH_KEY")
                .subscribeOn(Schedulers.io())
                .blockingGet();

        if (configModel != null)
            oauthModel = new Gson().fromJson(configModel.getValue(), OauthModel.class);

        OauthModel finalOauthModel = oauthModel;
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().addInterceptor(interceptor).addInterceptor(
                chain -> {

                    String authorization = null;

                    if (finalOauthModel != null)
                        authorization = String.valueOf(finalOauthModel.getAccessToken());

                    Request request = chain.request().newBuilder()
                            .addHeader("Authorization", "Bearer " + String.valueOf(authorization))
                            .addHeader("Accept", "application/json")
                            .build();

                    return chain.proceed(request);

                }
        ).build();


        Gson gson = new GsonBuilder()
                .setLenient()
                .create();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Config.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient.newBuilder().build()).build();

        return retrofit.create(ApiService.class);

    }

    private void addHeader(String header, String headerValue) {

        okHttpClient.newBuilder().addInterceptor(chain -> {

            Request request = chain.request().newBuilder()
                    .addHeader(header, headerValue)
                    .build();

            return chain.proceed(request);

        });

    }

}

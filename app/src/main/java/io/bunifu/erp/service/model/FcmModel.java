package io.bunifu.erp.service.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
@Entity(tableName = "notifications")
public class FcmModel {

    @PrimaryKey(autoGenerate = true)
    private Integer fcmId;

    @NonNull
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("student_id")
    @Expose
    private Integer studentId;
    @SerializedName("body")
    @Expose
    private String body;

    @SerializedName("payload")
    @Expose
    private String payload;

    @SerializedName("read_at")
    @Expose
    private String readAt;

    @SerializedName("received_at")
    @Expose
    private String receivedAt;
    @SerializedName("title")
    @Expose
    private String title;

    public Integer getFcmId() {
        return fcmId;
    }

    public void setFcmId(Integer fcmId) {
        this.fcmId = fcmId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public String getReadAt() {
        return readAt;
    }

    public void setReadAt(String readAt) {
        this.readAt = readAt;
    }

    public String getReceivedAt() {
        return receivedAt;
    }

    public void setReceivedAt(String receivedAt) {
        this.receivedAt = receivedAt;
    }
}
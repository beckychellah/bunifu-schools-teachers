package io.bunifu.erp.service.repository;

import android.app.Application;
import android.util.Log;

import com.google.gson.Gson;

import java.util.List;

import io.bunifu.erp.database.AppDatabase;
import io.bunifu.erp.database.DatabaseBuilder;
import io.bunifu.erp.service.ApiService;
import io.bunifu.erp.service.ServiceBuilder;
import io.bunifu.erp.service.model.ConfigModel;
import io.bunifu.erp.service.model.GuardianModel;
import io.bunifu.erp.service.model.StudentModel;
import io.reactivex.Flowable;
import io.reactivex.Observable;

public class UserRepository {

    private static String KEY_USER_PROFILE = "KEY_USER_PROFILE";
    private static String KEY_CURRENT_STUDENT = "KEY_CURRENT_STUDENT";
    private ApiService apiService;
    private AppDatabase db;
    RetrofitObservable retrofitObservable =new RetrofitObservable();

    public UserRepository(Application app) {
        this.apiService = ServiceBuilder.getRxService(app);
        this.db = DatabaseBuilder.getInstance(app).getDatabase();
    }

    public Observable<Boolean> init() {

        return apiService.fetchUserProfile()
                .flatMap(
                        guardianModel -> {
                            saveGuardianProfile(guardianModel);
                            //fetch students
                            return apiService.fetchStudents();
                        })
                //save students
                .flatMap(studentModels -> {

                    //save students
                    db.getStudentDao().insertAll(studentModels);

                    //set current student if value is not yet set
                    ConfigModel configModel = db.getConfigDao().getConfigSync(KEY_CURRENT_STUDENT);
                    if (configModel == null && studentModels.size() > 0) {
                        return  setCurrentStudent(studentModels.get(0))
                                .flatMap(aLong -> Flowable.just(true))
                                .toObservable();
                    }

                    return Observable.just(true);
                });

    }

    private void saveGuardianProfile(GuardianModel guardianModel) {
        //create a user profile config
        ConfigModel configModel = new ConfigModel();
        configModel.setKey(KEY_USER_PROFILE);
        configModel.setValue(new Gson().toJson(guardianModel, GuardianModel.class));
        //save guardian profile
        db.getConfigDao().insert(configModel);
    }

    public Flowable<GuardianModel> fetchUserProfile() {
        //fetch users profile from db
        return db.getConfigDao().getConfigASync(KEY_USER_PROFILE)
                .flatMap(configModel -> {
                            Log.e(getClass().getName(), configModel.getValue());
                            return Flowable.just(
                                    //convert stored config to guardian object
                                    new Gson().fromJson(configModel.getValue(), GuardianModel.class));
                        }
                ).map(guardianModel -> {
                    Log.e(getClass().getName(), "Guardian model Map function was called");
                    return guardianModel;
                });
    }

    public Flowable<StudentModel> fetchCurrentStudent() {
        return db.getConfigDao()
                .getConfigASync(KEY_CURRENT_STUDENT)
                .flatMap(
                        configModel ->
                        {
                            Log.e(getClass().getName(), "Flat map on current student");
                            return db.getStudentDao()
                                    .getStudentASync(Integer.parseInt(configModel.getValue()));
                        }
                );
    }

    public Flowable<Long> setCurrentStudent(StudentModel studentModel) {

        return Flowable.fromCallable(() -> {
            ConfigModel configModel = new ConfigModel();
            configModel.setKey(KEY_CURRENT_STUDENT);
            configModel.setValue(String.valueOf(studentModel.getId()));
            return db.getConfigDao().insertAsync(configModel);
        });

    }

    public Flowable<List<StudentModel>> fetchOtherStudents() {
        return fetchCurrentStudent()
                .flatMap(
                        studentModel ->
                                db.getStudentDao()
                                        .fetchAllStudentsExcept(studentModel.getId())
                );
    }
    public Flowable<List<StudentModel>> fetchStudentsFromDB(){
        return db.getStudentDao().fetchAllStudents();
    }

    public StudentModel getCurrentById(int studentId) {
        return db.getStudentDao()
                .getById(studentId)
                .subscribeOn(retrofitObservable.scheduler)
                .blockingGet();
    }
    public Observable<GuardianModel> updateProfile(GuardianModel guardianModel){
        return apiService.updateProfile(guardianModel)
                .flatMap(guardianModel1 -> {
                    saveGuardianProfile(guardianModel1);
                    return Observable.just(guardianModel1);
                });
    }

}

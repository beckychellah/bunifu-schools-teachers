package io.bunifu.erp.service.repository;

import android.app.Application;

import java.util.HashMap;
import java.util.Map;

import io.bunifu.erp.database.AppDatabase;
import io.bunifu.erp.database.DatabaseBuilder;
import io.bunifu.erp.service.ApiService;
import io.bunifu.erp.service.ServiceBuilder;
import io.bunifu.erp.service.model.SchoolModel;
import io.reactivex.Flowable;
import io.reactivex.Observable;

public class SchoolRepository {

    private ApiService apiService;
    private AppDatabase db;

    public SchoolRepository(Application app) {
        this.apiService = ServiceBuilder.getRxService(app);
        this.db = DatabaseBuilder.getInstance(app).getDatabase();
    }

    public Flowable<SchoolModel> fetchSchoolById(int schoolId) {
        Map<String, String> queryMap = new HashMap<>();
        queryMap.put("school_id", String.valueOf(schoolId));

        new RetrofitObservable<Boolean>().observe(
                apiService.fetchSchools(queryMap)
                        .flatMap(schoolModels -> {
                            db.getSchoolDao().insertAll(schoolModels);
                            return Observable.just(true);
                        })
        );

        return db.getSchoolDao().getSchoolASync(schoolId);

    }


}

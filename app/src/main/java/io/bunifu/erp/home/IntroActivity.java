package io.bunifu.erp.home;


import android.content.Intent;

import com.stephentuso.welcome.BasicPage;
import com.stephentuso.welcome.WelcomeActivity;
import com.stephentuso.welcome.WelcomeConfiguration;

import io.bunifu.erp.app.auth.LoginActivity;
import io.bunifu.erp.bunifuschools.R;


public class IntroActivity extends WelcomeActivity {

    @Override
    protected WelcomeConfiguration configuration() {
        return new WelcomeConfiguration.Builder(this)
                .defaultTitleTypefacePath("Montserrat-Bold.ttf")
                .defaultHeaderTypefacePath("Montserrat-Bold.ttf")
                .page(new BasicPage(R.drawable.results,
                        "Exam Results",
                        "Be on track with your child's performances across their various educational levels")
                        .background(R.color.white)
                )

                .page(new BasicPage(R.drawable.feestatements,
                        "Fee statements and structures",
                        " View fee structure breakdown and the fee balance you owe the school following the payments you make.")
                        .background(R.color.white)
                )

                .page(new BasicPage(R.drawable.meeting,
                        "Upcoming events",
                        "Never miss on any event going on in the school, their location and dates.")
                        .background(R.color.white)
                )

                .page(new BasicPage(R.drawable.start,
                        "",
                        "You don't want to miss this,Create an account with us.")
                        .background(R.color.white)
                )
                .swipeToDismiss(false)
                .build();


    }

    @Override
    protected void completeWelcomeScreen() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

}





package io.bunifu.erp.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import io.bunifu.erp.app.auth.LoginActivity;
import io.bunifu.erp.app.dashboard.DashActivity;
import io.bunifu.erp.service.repository.AuthRepository;


public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (new AuthRepository(getApplication()).isAuthenticated()) {
            startActivity(new Intent(this, DashActivity.class));
        } else {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }
    }


}


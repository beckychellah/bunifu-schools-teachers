package io.bunifu.erp.app.auth.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.hbb20.CountryCodePicker;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.bunifu.erp.app.auth.SignUpActivity;
import io.bunifu.erp.app.auth.viewModels.PhoneNoViewModel;
import io.bunifu.erp.app.auth.viewModels.WorkingViewModel;
import io.bunifu.erp.app.commons.stepper.interfaces.Step;
import io.bunifu.erp.bunifuschools.R;
import retrofit2.HttpException;

public class EnterPhoneNoFragment extends Fragment implements Step {

    @BindView(R.id.ccp)
    CountryCodePicker mCcp;

    @BindView(R.id.edt_phone_number)
    EditText mEdtPhoneNo;

    @BindView(R.id.btn_switch_mode)
    Button mBtnSwitchMode;

    @BindView(R.id.txi_phone_no)
    TextInputLayout mTxiPhoneNo;

    PhoneNoViewModel phoneNoViewModel;
    WorkingViewModel workingViewModel;

    private boolean isResetPasswordMode;
    private boolean isForwardNavigation = false;

    public EnterPhoneNoFragment() {
        // Required empty public constructor
    }


    public static EnterPhoneNoFragment newInstance() {
        return new EnterPhoneNoFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        phoneNoViewModel = ViewModelProviders.of(getActivity()).get(PhoneNoViewModel.class);
        workingViewModel = ViewModelProviders.of(getActivity()).get(WorkingViewModel.class);

        workingViewModel.isPasswordResetMode.observe(this, aBoolean -> isResetPasswordMode = aBoolean);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_enter_phone_no, container, false);
        ButterKnife.bind(this, view);

        mCcp.registerCarrierNumberEditText(mEdtPhoneNo);

        mEdtPhoneNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                phoneNoViewModel.setPhoneNo(mCcp.getFullNumber());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        phoneNoViewModel.getHttpObservable().loading.observe(this,
                aBoolean -> workingViewModel.setWorking(aBoolean)
        );

        phoneNoViewModel.getHttpObservable().response.observe(this, configModel -> {
            goToNext(true);
        });

        phoneNoViewModel.getHttpObservable().error.observe(this, stringHttpExceptionPair -> {
            if (stringHttpExceptionPair.second instanceof HttpException) {
                HttpException exception = stringHttpExceptionPair.second;
                if (exception.code() == 401) {
                    goToNext(false);
                    return;
                }
            }
            showError(stringHttpExceptionPair.first);

        });

        workingViewModel.isPasswordResetMode.observe(this,
                aBoolean -> {
                    mBtnSwitchMode.setText(
                            isResetPasswordMode
                                    ? "Sign up instead!"
                                    : "Reset password instead!"
                    );
                    mBtnSwitchMode.setVisibility(View.GONE);
                    mTxiPhoneNo.setErrorEnabled(false);
                }
        );

        mBtnSwitchMode.setOnClickListener(
                v -> {
                    //start sign up activity in the new mode
                    Intent intent = new Intent(getContext(), SignUpActivity.class);
                    //put mode as extra
                    intent.putExtra(getString(R.string.IS_PASSWORD_RESET_EXTRA), !isResetPasswordMode);
                    startActivity(intent);
                    getActivity().finish();
                }
        );

        return view;
    }

    public boolean verifyStep() {
        if (!mCcp.isValidFullNumber()) {
            mTxiPhoneNo.setError("The phone no is invalid");
        }
        mTxiPhoneNo.setErrorEnabled(!mCcp.isValidFullNumber());
        return mCcp.isValidFullNumber();
    }

    /**
     * @param exists Phone no exists in the server
     */
    private void goToNext(boolean exists) {

        phoneNoViewModel.setExists(exists);

        if ((exists && isResetPasswordMode) || (!exists && !isResetPasswordMode)) {
            if (!phoneNoViewModel.requestObserved) {
                phoneNoViewModel.requestObserved = true;
                workingViewModel.getStepperNavigation().goToNext();
            }

        } else {
            //show the error based on the mode we are in
            showError((
                    isResetPasswordMode
                            ? "The phone number does not exists"
                            : "This phone number is already taken"
            ));
            mBtnSwitchMode.setVisibility(View.VISIBLE);
        }

    }

    private void showError(String error) {
        Snackbar.make(mTxiPhoneNo, error, Snackbar.LENGTH_SHORT).show();
    }


    @Override
    public void onSelected(boolean isForwardNavigation) {
        this.isForwardNavigation = isForwardNavigation;
    }

    @Override
    public boolean onNextClicked() {
        //check for validation errors & if none check whether it exists
        if (verifyStep())
            phoneNoViewModel.checkIfPhoneNoExists();
        return false;
    }

    @Override
    public boolean onBackClicked() {
        return true;
    }
}

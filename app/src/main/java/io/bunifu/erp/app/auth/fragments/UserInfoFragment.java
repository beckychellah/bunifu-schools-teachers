package io.bunifu.erp.app.auth.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.bunifu.erp.app.auth.viewModels.UserInfoViewModel;
import io.bunifu.erp.app.auth.viewModels.WorkingViewModel;
import io.bunifu.erp.app.commons.stepper.interfaces.Step;
import io.bunifu.erp.bunifuschools.R;
import retrofit2.HttpException;


public class UserInfoFragment extends Fragment implements Step {


    @BindView(R.id.txi_full_name)
    TextInputLayout mTxiFullName;

    @BindView(R.id.txi_email)
    TextInputLayout mTxiEmail;

    UserInfoViewModel userInfoViewModel;
    WorkingViewModel workingViewModel;


    public UserInfoFragment() {
        // Required empty public constructor
    }

    public static UserInfoFragment newInstance() {
        return new UserInfoFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userInfoViewModel = ViewModelProviders.of(getActivity()).get(UserInfoViewModel.class);
        workingViewModel = ViewModelProviders.of(getActivity()).get(WorkingViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_info, container, false);
        ButterKnife.bind(this, view);

        mTxiFullName.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                userInfoViewModel.setFullName(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mTxiEmail.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                userInfoViewModel.setEmail(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        userInfoViewModel.getFullNameError().observe(this, error -> {
            mTxiFullName.setError(error);
            mTxiFullName.setErrorEnabled(!error.isEmpty());
        });

        userInfoViewModel.getEmailError().observe(this, error -> {
            mTxiEmail.setError(error);
            mTxiEmail.setErrorEnabled(!error.isEmpty());
        });

        userInfoViewModel.getHttpObservable().loading.observe(this, aBoolean ->
                workingViewModel.setWorking(aBoolean)
        );

        userInfoViewModel.getHttpObservable().response.observe(this, configModel ->
                userInfoViewModel.setEmailError("This email has already been taken"));

        userInfoViewModel.getHttpObservable().error.observe(this, stringHttpExceptionPair -> {
            if (stringHttpExceptionPair.second instanceof HttpException) {
                HttpException exception = stringHttpExceptionPair.second;
                if (exception.code() == 401) {
                    if (!userInfoViewModel.requestObserved)
                        workingViewModel.getStepperNavigation().goToNext();
                    userInfoViewModel.requestObserved = true;
                    return;
                }
            }
            showError(stringHttpExceptionPair.first);

        });

        return view;
    }

    private void showError(String first) {
        Snackbar.make(mTxiEmail, first, Snackbar.LENGTH_LONG).show();
    }


    @Override
    public boolean onNextClicked() {
        //check if the email has been taken
        if (userInfoViewModel.validate())
            userInfoViewModel.checkIfEmailExists();
        else {
            showError("Enter a valid name and email");
        }
        return false;
    }

    @Override
    public boolean onBackClicked() {
        return true;
    }

    @Override
    public void onSelected(boolean isForwardNavigation) {

    }
}

package io.bunifu.erp.app.auth.viewModels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import io.bunifu.erp.service.model.ConfigModel;
import io.bunifu.erp.service.repository.AuthRepository;
import io.bunifu.erp.service.repository.RetrofitObservable;
import io.reactivex.Observable;

public class PhoneNoViewModel extends AndroidViewModel {

    private AuthRepository authRepository;
    private MutableLiveData<String> phoneNo = new MutableLiveData<>();
    private MutableLiveData<Boolean> exists = new MutableLiveData<>();
    private RetrofitObservable<ConfigModel> httpObservable = new RetrofitObservable<>();

    public boolean requestObserved = false;

    public PhoneNoViewModel(@NonNull Application application) {
        super(application);
        authRepository = new AuthRepository(application);
    }

    public void checkIfPhoneNoExists() {
        requestObserved = false;
        Observable<ConfigModel> obs = authRepository.checkIfPhoneNoExists(String.valueOf(phoneNo.getValue()));
        httpObservable.observe(obs);
    }

    public void setExists(boolean exists) {
        this.exists.setValue(exists);
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo.setValue(phoneNo);
    }

    public RetrofitObservable<ConfigModel> getHttpObservable() {
        return httpObservable;
    }

    public MutableLiveData<String> getPhoneNo() {
        return phoneNo;
    }
}

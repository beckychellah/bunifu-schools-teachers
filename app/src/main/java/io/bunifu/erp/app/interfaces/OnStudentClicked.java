package io.bunifu.erp.app.interfaces;


public interface OnStudentClicked {
    void onClick(int studentId);
}

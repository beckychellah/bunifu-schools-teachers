package io.bunifu.erp.app.interfaces;

/**
 * Created by kenboi on 5/14/17.
 * Exams View Interface
 */

public interface IActivity {

    void showNoDataToDisplay(boolean show);

    void showError(String error);


}

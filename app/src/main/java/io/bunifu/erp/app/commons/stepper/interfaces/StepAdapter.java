package io.bunifu.erp.app.commons.stepper.interfaces;


import io.bunifu.erp.app.commons.stepper.adapters.MyPagerAdapter;

public interface StepAdapter {

    Step getItem(int position);

    int getCount();

    Step findStep(int position);

    MyPagerAdapter getPagerAdapter();

}

package io.bunifu.erp.app.auth.adapters;

import android.support.v4.app.FragmentManager;

import io.bunifu.erp.app.auth.fragments.EnterPhoneNoFragment;
import io.bunifu.erp.app.auth.fragments.UpdatePasswordFragment;
import io.bunifu.erp.app.auth.fragments.UserInfoFragment;
import io.bunifu.erp.app.auth.fragments.VerifyPhoneNoFragment;
import io.bunifu.erp.app.commons.stepper.adapters.AbstractStepperAdapter;
import io.bunifu.erp.app.commons.stepper.interfaces.Step;

public class StepperAdapter extends AbstractStepperAdapter {

    private boolean isPasswordReset;

    public StepperAdapter(FragmentManager fragmentManager, boolean isPasswordReset) {
        super(fragmentManager);
        this.isPasswordReset = isPasswordReset;
    }

    @Override
    public Step getItem(int position) {
        if (isPasswordReset) {
            switch (position) {
                case 0:
                    return EnterPhoneNoFragment.newInstance();
                case 1:
                    return VerifyPhoneNoFragment.newInstance();
                case 2:
                    return UpdatePasswordFragment.newInstance();
            }
        } else {

            switch (position) {
                case 0:
                    return EnterPhoneNoFragment.newInstance();
                case 1:
                    return VerifyPhoneNoFragment.newInstance();
                case 2:
                    return UserInfoFragment.newInstance();
                case 3:
                    return UpdatePasswordFragment.newInstance();
            }
        }

        return null;
    }

    @Override
    public int getCount() {
        return isPasswordReset ? 3 : 4;
    }


}

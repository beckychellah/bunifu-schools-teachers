package io.bunifu.erp.app.auth.viewModels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import io.bunifu.erp.service.model.OauthModel;
import io.bunifu.erp.service.repository.AuthRepository;
import io.bunifu.erp.service.repository.RetrofitObservable;

public class LoginViewModel extends AndroidViewModel {

    public RetrofitObservable<OauthModel> retrofitObservable = new RetrofitObservable<>();
    private AuthRepository authRepository;
    private MutableLiveData<String> emailOrPhone = new MutableLiveData<>();
    private MutableLiveData<Boolean> isValidPhone = new MutableLiveData<>();
    private MutableLiveData<String> password = new MutableLiveData<>();
    private MutableLiveData<Boolean> isValidPassword = new MutableLiveData<>();

    public LoginViewModel(@NonNull Application application) {
        super(application);
        authRepository = new AuthRepository(application);
    }

    public void doAuthenticate(boolean validFullNumber) {

        if (validate(validFullNumber)) {
            retrofitObservable.observe(authRepository.authenticateUser(emailOrPhone.getValue(), password.getValue()));
        }
    }

    private boolean validate(boolean validFullNumber) {

        //validate phone number
        isValidPhone.setValue(emailOrPhone.getValue()!=null&&validFullNumber);

        //validate password
        isValidPassword.setValue(password.getValue()!=null&&!String.valueOf(password.getValue()).isEmpty());

        //return va;id password and phone
        return isValidPassword.getValue() && isValidPhone.getValue();

    }

    public MutableLiveData<Boolean> getIsValidPhone() {
        return isValidPhone;
    }

    public MutableLiveData<Boolean> getIsValidPassword() {
        return isValidPassword;
    }

    public void setEmailOrPhone(String emailOrPhone, boolean isValid) {
        this.emailOrPhone.setValue(emailOrPhone);
        this.getIsValidPhone().setValue(isValid);
    }

    public void setPassword(String password) {
        this.password.setValue(password);
        this.isValidPassword.setValue(!password.isEmpty());
    }
}

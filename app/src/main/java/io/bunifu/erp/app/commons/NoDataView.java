package io.bunifu.erp.app.commons;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.bunifu.erp.bunifuschools.R;

public class NoDataView {

    @BindView(R.id.txt_title)
    TextView mTxtTitle;

    @BindView(R.id.txt_message)
    TextView mTxtMessage;

    @BindView(R.id.img_icon)
    ImageView mImageViewIcon;

    @BindView(R.id.btn_retry)
    public Button mBtnRetry;

    public NoDataView(View view) {
        ButterKnife.bind(this, view);
    }

    public void showNoData(String title, String message) {
        mTxtTitle.setText(title);
        mTxtMessage.setText(message);
    }

    public void showSetIcon(Context context, int iconRes) {
        mImageViewIcon.setImageDrawable(ContextCompat.getDrawable(context, iconRes));
    }
}

package io.bunifu.erp.app.interfaces;


public interface OnEventItemClicked {
    void onClick(long mId);
}

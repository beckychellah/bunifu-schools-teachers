package io.bunifu.erp.app.auth.viewModels;


import android.util.Log;

import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

class PhoneNoVerificationPresenter {

    private String TAG = "GETTING_STARTED";
    private FirebaseAuth mAuth;

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks stateChangedCallbacks;

    PhoneNoVerificationPresenter(PhoneAuthProvider.OnVerificationStateChangedCallbacks stateChangedCallbacks) {
        this.mAuth = FirebaseAuth.getInstance();
        this.stateChangedCallbacks = stateChangedCallbacks;
    }


    /**
     * @param phoneNumber Mobile phone no
     */
    void startPhoneNumberVerification(String phoneNumber) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                //phoneNumber = country code + phone number with the first zero trimmed out
                phoneNumber,
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                TaskExecutors.MAIN_THREAD,               // Activity (for callback binding)
                stateChangedCallbacks);        // OnVerificationStateChangedCallbacks

    }


    void resendVerificationCode(String phoneNumber,
                                PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                TaskExecutors.MAIN_THREAD,               // Activity (for callback binding)
                stateChangedCallbacks,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks
        
    }


    /**
     * Called after the user enters the verification code received as sms
     *
     * @param code Verification Code
     */
    void verifyPhoneNumberWithCode(String code, String verificationId) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        signInWithPhoneAuthCredential(credential);
    }


    String handleErrors(Exception e) {
        Log.e(TAG, "onVerificationFailed, was called", e);
        //set default error message
        String error = "Sorry we are unable to process your request right now, please try again later.";
        if (e instanceof FirebaseAuthInvalidCredentialsException) {
            error = "The sms verification code used is invalid.";
        } else if (e instanceof FirebaseTooManyRequestsException) {
            //Quota exceeded
            //implement our own custom verification
        } else if (e instanceof FirebaseNetworkException) {
            error = "Please ensure your internet connection is active then try again.";
        }

        return error;

    }


    private void signInWithPhoneAuthCredential(final PhoneAuthCredential credential) {

        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(TaskExecutors.MAIN_THREAD, task -> {
                    if (task.isSuccessful()) {
                        stateChangedCallbacks.onVerificationCompleted(credential);
                    } else {
                        stateChangedCallbacks.onVerificationFailed((FirebaseException) task.getException());
                    }
                });
    }


}

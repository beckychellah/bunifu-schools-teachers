package io.bunifu.erp.app.interfaces;


import io.bunifu.erp.service.model.FcmModel;

public interface OnEventNotClicked {
    void onClick(FcmModel fcmModel);
}

package io.bunifu.erp.app.auth;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hbb20.CountryCodePicker;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.bunifu.erp.app.auth.viewModels.LoginViewModel;

import io.bunifu.erp.app.dashboard.DashActivity;
import io.bunifu.erp.bunifuschools.R;


public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.txi_phone_no)
    TextInputLayout mTxiPhoneNo;

    @BindView(R.id.txi_password)
    TextInputLayout mTxiPassword;

    @BindView(R.id.loadingRelativeLayout)
    RelativeLayout mLoadingLayout;

    @BindView(R.id.ccp)
    CountryCodePicker mCcp;

    @BindView(R.id.btn_login)
    Button mLogin;

    @BindView(R.id.btn_get_started)
    Button mBtnGetStarted;

    @BindView(R.id.text_reset_password)
    TextView mResetPassword;

    LoginViewModel loginViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        mCcp.registerCarrierNumberEditText(mTxiPhoneNo.getEditText());


        //initialise login presenter
        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);

        //validators
        loginViewModel.getIsValidPhone().observe(this, this::showInvalidEmailMessageOrPhone);
        loginViewModel.getIsValidPassword().observe(this, this::showInvalidPasswordMessage);

        //http request
        loginViewModel.retrofitObservable.error.observe(this, stringHttpExceptionPair -> {

            String error = stringHttpExceptionPair.first;

            if (stringHttpExceptionPair.second != null
                    && stringHttpExceptionPair.second.code() == 401)
                error = "Incorrect phone or password";

            loginError(error);

        });

        //loading
        loginViewModel.retrofitObservable.loading.observe(this, show -> showLoadingProgressBar(show));

        //success
        loginViewModel.retrofitObservable.response.observe(this, oauthModel -> loginSuccess());

        //add text changed listener
        mTxiPhoneNo.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                loginViewModel.setEmailOrPhone(mCcp.getFullNumber(), mCcp.isValidFullNumber());
            }
        });
        mTxiPassword.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                loginViewModel.setPassword(s.toString());

            }
        });

    }
    @SuppressLint("SetTextI18n")
    public void showInvalidEmailMessageOrPhone(boolean valid) {
        mTxiPhoneNo.setError("Invalid phone number");
        mTxiPhoneNo.setErrorEnabled(!valid);
    }

    public void showInvalidPasswordMessage(boolean valid) {
        mTxiPassword.setError("Enter a valid password");
        mTxiPassword.setErrorEnabled(!valid);
    }

    public void showLoadingProgressBar(boolean show) {
        mLoadingLayout.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public void loginSuccess() {
        mLoadingLayout.setVisibility(View.GONE);
        startActivity(new Intent(this, DashActivity.class));
        finish();
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    public void loginError(String error) {
        mLoadingLayout.setVisibility(View.GONE);
        Snackbar.make(mLogin, error, Snackbar.LENGTH_LONG).show();
    }

    @OnClick(R.id.btn_login)
    public void doLogin() {
        loginViewModel.doAuthenticate(mCcp.isValidFullNumber());
    }

    @OnClick(R.id.btn_get_started)
    public void getStarted() {
        Intent intent = new Intent(this, SignUpActivity.class);
        intent.putExtra(getString(R.string.IS_PASSWORD_RESET_EXTRA), false);
        startActivity(intent);
    }

    @OnClick(R.id.text_reset_password)
    public void resetPassword() {
        Intent intent = new Intent(this, SignUpActivity.class);
        intent.putExtra(getString(R.string.IS_PASSWORD_RESET_EXTRA), true);
        startActivity(intent);
    }
}

package io.bunifu.erp.app.interfaces;

import io.bunifu.erp.service.model.Exam;

public interface OnExamItemClicked {
    void onClick(Exam exam);
}

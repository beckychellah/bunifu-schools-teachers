package io.bunifu.erp.app.attendance.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.bunifu.erp.app.attendance.StudentsListActivity;
import io.bunifu.erp.app.attendance.data.LessonsModel;
import io.bunifu.erp.app.discipline.DisciplineActivity;
import io.bunifu.erp.bunifuschools.R;

public class LessonsAdapter extends RecyclerView.Adapter<LessonsAdapter.MyViewHolder> {

    List<LessonsModel> lessonsModels = new ArrayList<>();
    private Context context;


    public LessonsAdapter(Context context) {
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lesson_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        LessonsModel lessonsModel = lessonsModels.get(position);
        holder.getmSubject.setText(lessonsModel.getSubject());
        //  holder.getClass.setText(lessonsModel.get_class());
        holder.mStartTime.setText(lessonsModel.getStartTime());
        holder.mEndTime.setText(lessonsModel.getEndTime());
        holder.linearLayout.setOnClickListener(v -> {
        });

    }


    @Override
    public int getItemCount() {
        if (lessonsModels != null)
            return lessonsModels.size();
        else
            return 0;
    }

    public void updateLessons(List<LessonsModel> lessonsModels) {
        this.lessonsModels = lessonsModels;
        notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener,View.OnClickListener,
            MenuItem.OnMenuItemClickListener {
        @BindView(R.id.linearLayout)
        LinearLayout linearLayout;
        @BindView(R.id.txt_subject)
        TextView mSubject;
        @BindView(R.id.txt_time)
        TextView mTime;
        @BindView(R.id.txt_class)
        TextView mClass;

        @BindView(R.id.txt_value)
        TextView getmSubject;
        @BindView(R.id.txt_classValue)
        TextView getClass;
        @BindView(R.id.startTime)
        TextView mStartTime;
        @BindView(R.id.endTime)
        TextView mEndTime;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnCreateContextMenuListener(this);
            itemView.setOnClickListener(this);
            context=itemView.getContext();
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            menu.setHeaderTitle("Select the Action");
            SubMenu teacher = menu.addSubMenu(Menu.NONE, 1, 4,"Mark my Attendance");
            teacher.add(1, 2, 1, "Attended");
            teacher.add(1, 3, 2, "Not Attended");
            teacher.setGroupCheckable(1,true,true);
          MenuItem student =  menu.add(ContextMenu.NONE, 4, 2, "Mark students Attendance");
          student.setOnMenuItemClickListener(this);
        }

        @Override
        public void onClick(View v) {


        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()){

                case 1:
                    //do some stuff
                    break;

                case 2:
                    //submenu
                    if (item.isChecked()) item.setChecked(false);
                    else item.setChecked(true);
                    return true;

                case 3:
                    //submenu
                    if (item.isChecked()) item.setChecked(false);
                    else item.setChecked(true);
                    return true;
                case 4:
                    Integer classValue = new Gson().toJson()
                    Intent intent = new Intent(context, StudentsListActivity.class);
                    context.startActivity(intent);

                    break;

            }

            return true;
        }
    }
}

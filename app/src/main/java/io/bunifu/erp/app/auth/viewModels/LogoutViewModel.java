package io.bunifu.erp.app.auth.viewModels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;

import io.bunifu.erp.service.repository.AuthRepository;

public class LogoutViewModel extends AndroidViewModel {
    private AuthRepository authRepository;

    public LogoutViewModel(@NonNull Application application) {
        super(application);
        authRepository = new AuthRepository(application);
    }



}

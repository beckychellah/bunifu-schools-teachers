package io.bunifu.erp.app.attendance.data;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;


@Entity(tableName = "lessons")
public class LessonsModel {
    @NonNull
    @PrimaryKey
    private Integer _class;
    private String startTime;
    private String endTime;
    private String subject;


    public LessonsModel(String startTime, String endTime, String subject, Integer _class) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.subject = subject;
        this._class = _class;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Integer get_class() {
        return _class;
    }

    public void set_class(Integer _class) {
        this._class = _class;
    }
}

package io.bunifu.erp.app.attendance;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.bunifu.erp.app.attendance.adapters.LessonsAdapter;
import io.bunifu.erp.app.attendance.viewModels.LessonsViewModel;
import io.bunifu.erp.bunifuschools.R;

public class SubjectsActivity extends AppCompatActivity {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    LessonsAdapter adapter;
    LessonsViewModel lessonsViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subjects);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Timetable");

        adapter = new LessonsAdapter(getApplicationContext());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        registerForContextMenu(recyclerView);


        lessonsViewModel = ViewModelProviders.of(this).get(LessonsViewModel.class);
        lessonsViewModel.getAllLessons().observe(this, lessonsModel -> {
            Log.e(getClass().getName(), "The lessons are:"+ lessonsModel.size());
            if (lessonsModel != null)
                adapter.updateLessons(lessonsModel);

        });

    }

}

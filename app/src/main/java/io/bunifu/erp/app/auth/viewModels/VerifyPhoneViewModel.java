package io.bunifu.erp.app.auth.viewModels;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.google.firebase.FirebaseException;
import com.google.firebase.auth.PhoneAuthProvider;

public class VerifyPhoneViewModel extends ViewModel {

    public MutableLiveData<String> phone = new MutableLiveData<>();
    public MutableLiveData<String> error = new MutableLiveData<>();
    public MutableLiveData<Boolean> verificationStatus = new MutableLiveData<>();

    private PhoneNoVerificationPresenter presenter;
    private String verificationId;
    private PhoneAuthProvider.ForceResendingToken forceResendingToken;

    public void sendVerificationCode(PhoneAuthProvider.OnVerificationStateChangedCallbacks
                                             stateChangedCallbacks) {
        if (presenter == null)
            presenter = new PhoneNoVerificationPresenter(stateChangedCallbacks);

        presenter.startPhoneNumberVerification(phone.getValue());
    }

    public void verifyCode(String code) {
        presenter.verifyPhoneNumberWithCode(code, verificationId);
    }

    public void setException(FirebaseException e) {
        error.setValue(presenter.handleErrors(e));
    }

    public void resendVerificationCode() {
        presenter.resendVerificationCode(phone.getValue(), forceResendingToken);
    }

    public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
        this.verificationId = verificationId;
        this.forceResendingToken = forceResendingToken;
    }

    public MutableLiveData<Boolean> getVerificationStatus() {
        return verificationStatus;
    }

    public void setVerificationStatus(boolean verificationStatus) {
        this.verificationStatus.setValue(verificationStatus);
    }
}

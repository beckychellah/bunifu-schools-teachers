package io.bunifu.erp.app.auth;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.RelativeLayout;

import com.afollestad.materialdialogs.MaterialDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.bunifu.erp.app.auth.adapters.StepperAdapter;
import io.bunifu.erp.app.auth.viewModels.WorkingViewModel;
import io.bunifu.erp.app.commons.stepper.StepperLayout;
import io.bunifu.erp.bunifuschools.R;

public class SignUpActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.stepper_layout)
    RelativeLayout mStepperLayout;

    //view models
    WorkingViewModel workingViewModel;

    //adapters
    StepperLayout stepperLayout;
    //progress dialog
    private MaterialDialog materialDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_started);

        ButterKnife.bind(this);
        //set toolbar
        setSupportActionBar(toolbar);
        //set action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //view models
        workingViewModel = ViewModelProviders.of(this).get(WorkingViewModel.class);

        //get extra
        boolean isPasswordResetMode = getIntent().getBooleanExtra(getString(R.string.IS_PASSWORD_RESET_EXTRA), false);

        getSupportActionBar().setTitle(isPasswordResetMode ? "Reset Password" : "Sign Up");
        stepperLayout = new StepperLayout(mStepperLayout);
        stepperLayout.setAdapter(new StepperAdapter(getSupportFragmentManager(), isPasswordResetMode));
        workingViewModel.setStepperNavigation(stepperLayout.getStepperNavigation());


        workingViewModel.isPasswordResetMode.setValue(isPasswordResetMode);

        //loading
        workingViewModel.getWorking().observe(this, this::showLoading);


    }

    private void showLoading(Boolean aBoolean) {
        if (materialDialog == null)
            materialDialog = new MaterialDialog.Builder(this)
                    .content("Loading")
                    .cancelable(false)
                    .progress(true, 0)
                    .build();

        if (aBoolean)
            materialDialog.show();
        else
            materialDialog.dismiss();
    }

}

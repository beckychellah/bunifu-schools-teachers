package io.bunifu.erp.app.auth.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.bunifu.erp.app.auth.viewModels.PhoneNoViewModel;
import io.bunifu.erp.app.auth.viewModels.VerifyPhoneViewModel;
import io.bunifu.erp.app.auth.viewModels.WorkingViewModel;
import io.bunifu.erp.app.commons.stepper.interfaces.Step;
import io.bunifu.erp.bunifuschools.R;


public class VerifyPhoneNoFragment extends Fragment implements Step {

    public static final String TAG = "VERIFY_PHONE_NO";

    @BindView(R.id.txt_phone_no)
    TextView mTxtPhoneNo;

    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    @BindView(R.id.txt_remaining_time)
    TextView mTxtRemainingTime;

    @BindView(R.id.edt_code)
    EditText mEdtCode;

    @BindView(R.id.btn_resend_sms)
    Button mBtnResendSms;

    private PhoneNoViewModel phoneNoViewModel;
    private WorkingViewModel workingViewModel;
    private VerifyPhoneViewModel verifyPhoneViewModel;

    private int counter = 0;
    private CountDownTimer mCountDownTimer;
    //flags
    //checks whether the fragment has been paused
    private boolean paused = true;
    //verification process in progress
    private boolean timedOut = false;

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks
            stateChangedCallbacks
            = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(verificationId, forceResendingToken);
            startCountDownTimer(true);
            verifyPhoneViewModel.onCodeSent(verificationId, forceResendingToken);
            workingViewModel.setWorking(false);
        }

        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            workingViewModel.setWorking(false);
            //update request in progress flag to true
            timedOut = true;
            workingViewModel.getStepperNavigation().goToNext();
            Toast.makeText(getContext(), "Verification complete", Toast.LENGTH_SHORT).show();
            //stop timer
            startCountDownTimer(false);
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Log.e(TAG, "onVerificationFailed: was called");
            verifyPhoneViewModel.setException(e);
            timedOut = true;
            //stop timer
            startCountDownTimer(false);
        }

        @Override
        public void onCodeAutoRetrievalTimeOut(String s) {
            timedOut = true;
        }
    };


    public VerifyPhoneNoFragment() {
    }

    public static VerifyPhoneNoFragment newInstance() {
        return new VerifyPhoneNoFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        phoneNoViewModel = ViewModelProviders.of(getActivity()).get(PhoneNoViewModel.class);
        workingViewModel = ViewModelProviders.of(getActivity()).get(WorkingViewModel.class);
        verifyPhoneViewModel = ViewModelProviders.of(getActivity()).get(VerifyPhoneViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_verify_phone_no, container, false);
        ButterKnife.bind(this, view);

        mTxtRemainingTime.setText(String.valueOf("1:30"));
        mBtnResendSms.setEnabled(false);

        phoneNoViewModel.getPhoneNo().observe(this, mPhoneNo -> {
            mTxtPhoneNo.setText(Html.fromHtml(String.valueOf(getString(R.string.text_sent_msg) + " " + "<b>+" + mPhoneNo + "</b> " + ".")));
            verifyPhoneViewModel.phone.setValue("+" + mPhoneNo);
        });

        mEdtCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String code = s.toString();
                if (code.length() == 6) {
                    //TODO verify phone with code
                    verifyPhoneViewModel.verifyCode(code);
                    //show loading
                    workingViewModel.setWorking(true);
                }
            }
        });

        verifyPhoneViewModel.error.observe(this, s -> {
            Snackbar.make(mBtnResendSms, s, Snackbar.LENGTH_LONG).show();
            workingViewModel.setWorking(false);
        });

        return view;
    }

    @OnClick(R.id.btn_resend_sms)
    public void onResend() {
        verifyPhoneViewModel.resendVerificationCode();
        startCountDownTimer(true);
    }

    private void startCountDownTimer(boolean start) {

        //reset progress
        counter = 0;

        //cancel the timer if its running
        if (mCountDownTimer != null)
            mCountDownTimer.cancel();

        if (start) {
            mCountDownTimer = new CountDownTimer((90 * 1000), 1000) {

                @Override
                public void onTick(long millisUntilFinished) {
                    //update counter
                    counter++;
                    //set progress
                    mProgressBar.setProgress(counter);
                    //update remaining time
                    mTxtRemainingTime.setText(formatTime(millisUntilFinished));
                }

                @Override
                public void onFinish() {
                    mBtnResendSms.setEnabled(true);
                    mEdtCode.setEnabled(true);
                    //set remaining time to zero
                    mTxtRemainingTime.setText(formatTime(0));
                    //set progress to complete
                    mProgressBar.setProgress(90);
                }
            };
            mCountDownTimer.start();
            mBtnResendSms.setEnabled(false);
        } else {
            //set progress
            mProgressBar.setProgress(counter);
            //update remaining time
            mTxtRemainingTime.setText(formatTime(0));
            //enable button
            mBtnResendSms.setEnabled(true);
        }
    }

    /**
     * @param milliseconds time duration
     * @return milliseconds formatted as min:sec
     */
    private String formatTime(long milliseconds) {

        int seconds = (int) ((milliseconds / 1000) % 60);
        int minutes = (int) ((milliseconds / 1000) / 60);

        return String.valueOf(minutes + ":" + (seconds > 9 ? "" : 0) + seconds);
    }

    @Override
    public void onSelected(boolean isForwardNavigation) {
        //start the verification code process if its forward navigation
        if (isForwardNavigation) {
            verifyPhoneViewModel.sendVerificationCode(stateChangedCallbacks);
            timedOut = false;
        } else {
            //re enable resend sms button
            mBtnResendSms.setEnabled(true);
        }
    }

    @Override
    public boolean onNextClicked() {
        Snackbar.make(mBtnResendSms, "Please wait, you'll be automatically redirected.", Snackbar.LENGTH_SHORT).show();
        return false;
    }

    @Override
    public boolean onBackClicked() {
        if (!timedOut)
            Snackbar.make(mBtnResendSms, "Please wait, verification in progress", Snackbar.LENGTH_SHORT).show();
        return timedOut;
    }

}

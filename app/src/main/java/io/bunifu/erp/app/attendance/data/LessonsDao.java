package io.bunifu.erp.app.attendance.data;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;


@Dao
public interface LessonsDao {

    @Query("SELECT * FROM lessons")
    LiveData<List<LessonsModel>> getLessons();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
     void insert(LessonsModel lessonsModels);

    @Query("DELETE FROM lessons")
    void delete();

}

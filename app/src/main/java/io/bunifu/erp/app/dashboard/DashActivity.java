package io.bunifu.erp.app.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.bunifu.erp.app.attendance.SubjectsActivity;
import io.bunifu.erp.app.communication.ForumsActivity;
import io.bunifu.erp.app.discipline.DisciplineActivity;
import io.bunifu.erp.bunifuschools.R;

public class DashActivity extends AppCompatActivity {
    @BindView(R.id.ll_attendance)
    LinearLayout mAttendance;
    @BindView(R.id.ll_discipline)
    LinearLayout mllDiscipline;
    @BindView(R.id.ll_comm)
    LinearLayout mllCommunication;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashb);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Dashboard");

        mAttendance.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), SubjectsActivity.class);
            startActivity(intent);
        });
        mllDiscipline.setOnClickListener(v ->
                startActivity(new Intent(getApplicationContext(), DisciplineActivity.class)));
        mllCommunication.setOnClickListener(v ->
                startActivity(new Intent(getApplicationContext(), ForumsActivity.class)));
    }
}

package io.bunifu.erp.app.attendance.data;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

/**
 * Created by User on 23/04/2018.
 */
@Database(entities = {LessonsModel.class}, version=3)
public abstract class LessonsRoomDatabase extends RoomDatabase {

    public abstract LessonsDao getlessonsDao();

    private static LessonsRoomDatabase INSTANCE;

    static LessonsRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (LessonsRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            LessonsRoomDatabase.class, "lessons_database")
                            // Wipes and rebuilds instead of migrating if no Migration object.
                            // Migration is not part of this codelab.
                            .fallbackToDestructiveMigration()
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    /**
     * Override the onOpen method to populate the database.
     * For this sample, we clear the database every time it is created or opened.
     */
    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback(){

        @Override
        public void onOpen (@NonNull SupportSQLiteDatabase db){
            super.onOpen(db);
            // If you want to keep the data through app restarts,
            // comment out the following line.
            new PopulateDbAsync(INSTANCE).execute();
        }
    };


    /**
     * Populate the database in the background.
     * If you want to start with more words, just add them.
     */
    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final LessonsDao mDao;

        PopulateDbAsync(LessonsRoomDatabase db) {
            mDao = db.getlessonsDao();
        }

        @Override
        protected Void doInBackground(final Void... params) {
            // Start the app with a clean database every time.
            // Not needed if you only populate on creation.
            mDao.delete();

            LessonsModel lessonsModel = new LessonsModel("11:00", "12:45", "English", 2);
            mDao.insert(lessonsModel);
            lessonsModel = new LessonsModel("3:00pm", "4:20pm", "Science", 4);
            mDao.insert(lessonsModel);
            lessonsModel = new LessonsModel("1:00pm", "2:20pm", "Kiswahili", 1);
            mDao.insert(lessonsModel);
            lessonsModel = new LessonsModel("5:00pm", "6:20pm", "Social Studies", 3);
            mDao.insert(lessonsModel);
            lessonsModel = new LessonsModel("2:30pm", "4:00pm", "Art", 6);
            mDao.insert(lessonsModel);
            return null;
        }
    }

}

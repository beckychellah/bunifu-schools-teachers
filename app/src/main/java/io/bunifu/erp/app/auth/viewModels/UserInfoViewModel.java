package io.bunifu.erp.app.auth.viewModels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import io.bunifu.erp.service.model.ConfigModel;
import io.bunifu.erp.service.repository.AuthRepository;
import io.bunifu.erp.service.repository.RetrofitObservable;
import io.bunifu.erp.utils.extras.EmailValidator;
import io.reactivex.Observable;


public class UserInfoViewModel extends AndroidViewModel {

    //flag to check if its been observed
    public boolean requestObserved = false;

    private MutableLiveData<String> fullName = new MutableLiveData<>();
    private MutableLiveData<String> fullNameError = new MutableLiveData<>();
    private MutableLiveData<String> email = new MutableLiveData<>();
    private MutableLiveData<String> emailError = new MutableLiveData<>();
    private RetrofitObservable<ConfigModel> httpObservable = new RetrofitObservable<>();
    private AuthRepository authRepository;

    public UserInfoViewModel(@NonNull Application application) {
        super(application);
        authRepository = new AuthRepository(application);
    }


    public MutableLiveData<String> getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName.setValue(fullName);
        String error = "";
        if (fullName.length() == 0) {
            error = "Enter a valid name";
        }
        fullNameError.setValue(error);
    }

    public MutableLiveData<String> getFullNameError() {
        return fullNameError;
    }

    public MutableLiveData<String> getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email.setValue(email);
        String error = "";
        if (!EmailValidator.getInstance().isValid(email)) {
            error = "Enter a valid email";
        }
        emailError.setValue(error);

    }

    public MutableLiveData<String> getEmailError() {
        return emailError;
    }

    public void setEmailError(String emailError) {
        this.emailError.setValue(emailError);
    }

    public boolean validate() {
        return String.valueOf(fullName.getValue()).length() > 0
                && EmailValidator.getInstance().isValid(String.valueOf(email.getValue()));
    }

    public RetrofitObservable<ConfigModel> getHttpObservable() {
        return httpObservable;
    }

    public void checkIfEmailExists() {
        requestObserved = false;
        Observable<ConfigModel> obs = authRepository.checkIfEmailExists(String.valueOf(email.getValue()));
        httpObservable.observe(obs);
    }
}

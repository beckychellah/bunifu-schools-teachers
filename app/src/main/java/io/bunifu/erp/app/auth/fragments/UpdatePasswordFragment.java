package io.bunifu.erp.app.auth.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ybs.passwordstrengthmeter.PasswordStrength;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.bunifu.erp.app.auth.LoginActivity;
import io.bunifu.erp.app.auth.viewModels.PasswordViewModel;
import io.bunifu.erp.app.auth.viewModels.PhoneNoViewModel;
import io.bunifu.erp.app.auth.viewModels.UserInfoViewModel;
import io.bunifu.erp.app.auth.viewModels.WorkingViewModel;
import io.bunifu.erp.app.commons.stepper.interfaces.Step;
import io.bunifu.erp.bunifuschools.R;


public class UpdatePasswordFragment extends Fragment implements Step {


    @BindView(R.id.txi_password)
    TextInputLayout mTxiPassword;

    @BindView(R.id.txi_confirm_password)
    TextInputLayout mTxiConfirmPassword;

    @BindView(R.id.txt_strength)
    TextView strengthView;

    @BindView(R.id.layout_pwd_strength)
    LinearLayout linearLayoutPwdStrength;

    WorkingViewModel workingViewModel;
    PasswordViewModel passwordViewModel;
    PhoneNoViewModel phoneNoViewModel;
    UserInfoViewModel userInfoViewModel;


    public UpdatePasswordFragment() {
        // Required empty public constructor
    }

    public static UpdatePasswordFragment newInstance() {
        return new UpdatePasswordFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        workingViewModel = ViewModelProviders.of(getActivity()).get(WorkingViewModel.class);
        passwordViewModel = ViewModelProviders.of(getActivity()).get(PasswordViewModel.class);
        phoneNoViewModel = ViewModelProviders.of(getActivity()).get(PhoneNoViewModel.class);
        userInfoViewModel = ViewModelProviders.of(getActivity()).get(UserInfoViewModel.class);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_update_password, container, false);
        ButterKnife.bind(this, view);

        mTxiPassword.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                passwordViewModel.setPassword(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mTxiConfirmPassword.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                passwordViewModel.setConfirmPassword(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        passwordViewModel.getPassword().observe(this, this::updatePasswordStrengthView);

        passwordViewModel.getPasswordError().observe(this, error -> {
            mTxiPassword.setError(error);
            mTxiPassword.setErrorEnabled(!error.isEmpty());
        });

        passwordViewModel.getConfirmPasswordError().observe(this, error -> {
            mTxiConfirmPassword.setError(error);
            mTxiConfirmPassword.setErrorEnabled(!error.isEmpty());
        });

        //loading
        workingViewModel.getRetrofitObservable().loading.observe(this, aBoolean ->
                workingViewModel.setWorking(true)
        );

        //request complete
        workingViewModel.getRetrofitObservable().response.observe(this, configModel -> {
            Toast.makeText(getContext(), "Success, proceed to login", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(getContext(), LoginActivity.class);
            startActivity(intent);
            getActivity().finish();
        });

        workingViewModel.getRetrofitObservable().error.observe(this,
                stringHttpExceptionPair -> {
                    workingViewModel.setWorking(false);
                    Snackbar.make(mTxiConfirmPassword, stringHttpExceptionPair.first, Snackbar.LENGTH_LONG).show();
                }
        );

        return view;
    }


    private void updatePasswordStrengthView(String password) {

        if (TextView.VISIBLE != strengthView.getVisibility())
            return;

        linearLayoutPwdStrength.setVisibility(
                password.isEmpty()
                        ? View.GONE
                        : View.VISIBLE
        );

        if (password.isEmpty()) {
            return;
        }

        PasswordStrength str = PasswordStrength.calculateStrength(password);
        strengthView.setText(String.valueOf(str.getText(getContext())).toLowerCase());

    }


    /**
     * execute on completed sequence of events
     */
    public void onCompleted() {
        Map<String, String> params = new HashMap<>();
        params.put("name", String.valueOf(userInfoViewModel.getFullName().getValue()));
        params.put("email", String.valueOf(userInfoViewModel.getEmail().getValue()));
        params.put("phone", String.valueOf(phoneNoViewModel.getPhoneNo().getValue()));
        params.put("password", String.valueOf(passwordViewModel.getPassword().getValue()));
        workingViewModel.onCompleted(params);
    }

    @Override
    public boolean onNextClicked() {
        if (passwordViewModel.validate())
            onCompleted();
        else
            Snackbar.make(mTxiPassword, "Invalid password", Snackbar.LENGTH_SHORT).show();
        return false;
    }

    @Override
    public boolean onBackClicked() {
        return true;
    }

    @Override
    public void onSelected(boolean isForwardNavigation) {

    }
}

package io.bunifu.erp.app.attendance.data;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.os.AsyncTask;

import java.util.List;

/**
 * Created by User on 19/04/2018.
 */

public class LessonsRepository {
    private LessonsDao lessonsDao;
    Context context;

    public LessonsRepository(Application app) {
        LessonsRoomDatabase db = LessonsRoomDatabase.getDatabase(app);
        lessonsDao =db.getlessonsDao();
    }
    public void generateData() {
        for (int i = 0; i < 5; ) {
            LessonsModel lessonsModel = new LessonsModel("11:00", "12:45", "English", 2);
            insertAll(lessonsModel);
            lessonsModel = new LessonsModel("3:00pm", "4:20pm", "Science", 4);
            insertAll(lessonsModel);
        }
    }
    public LiveData<List<LessonsModel>> getAllLessons() {
        return lessonsDao.getLessons();
    }

    public void insertAll(LessonsModel lessonsModels) {
        new insertAsyncTask(lessonsDao).execute(lessonsModels);
    }
    private static class insertAsyncTask extends AsyncTask<LessonsModel, Void, Void> {

        private LessonsDao lessonsDao1;

        insertAsyncTask(LessonsDao dao) {
            lessonsDao1 = dao;
        }

        @Override
        protected Void doInBackground(LessonsModel... lessonsModels) {
            lessonsDao1.insert(lessonsModels[0]);
            return null;
        }
    }



}

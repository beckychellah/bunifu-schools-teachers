package io.bunifu.erp.app.auth.viewModels;


import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import java.util.Map;

import io.bunifu.erp.app.commons.stepper.StepperLayout;
import io.bunifu.erp.service.model.ConfigModel;
import io.bunifu.erp.service.repository.AuthRepository;
import io.bunifu.erp.service.repository.RetrofitObservable;

public class WorkingViewModel extends AndroidViewModel {

    public MutableLiveData<Boolean> isPasswordResetMode = new MutableLiveData<>();
    private MutableLiveData<Boolean> working = new MutableLiveData<>();
    private AuthRepository authRepository;

    private RetrofitObservable<ConfigModel> retrofitObservable = new RetrofitObservable<>();

    //Navigation
    StepperLayout.StepperNavigation stepperNavigation;


    public WorkingViewModel(@NonNull Application application) {
        super(application);
        this.authRepository = new AuthRepository(application);
    }

    public MutableLiveData<Boolean> getWorking() {
        return working;
    }

    public void setWorking(boolean working) {
        this.working.setValue(working);
    }

    public RetrofitObservable<ConfigModel> getRetrofitObservable() {
        return retrofitObservable;
    }

    public StepperLayout.StepperNavigation getStepperNavigation() {
        return stepperNavigation;
    }

    public void setStepperNavigation(StepperLayout.StepperNavigation stepperNavigation) {
        this.stepperNavigation = stepperNavigation;
    }

    public void onCompleted(Map<String, String> params) {
        if (isPasswordResetMode.getValue()) {
            retrofitObservable.observe(authRepository.resetPassword(params));
        } else {
            retrofitObservable.observe(authRepository.signUp(params));
        }
    }

}

package io.bunifu.erp.app.commons.stepper.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import java.util.ArrayList;
import java.util.List;

import io.bunifu.erp.app.commons.stepper.interfaces.Step;

/*
*Create view pager adapter
* This is just a standard pager fragment adapter
*/
public class MyPagerAdapter extends MyFragmentPagerAdapter {

    //instances of a class which extends fragment
    // and implements the step interface
    private List<Step> steps = new ArrayList<>();
    private FragmentManager fragmentManager;

    MyPagerAdapter(FragmentManager fragmentManager, List<Step> steps) {
        super(fragmentManager);
        this.steps = steps;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public Fragment getItem(int position) {
        return (Fragment) steps.get(position);
    }

    @Override
    public int getCount() {
        return steps.size();
    }

}

package io.bunifu.erp.app.commons.stepper;


import android.os.Build;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.bunifu.erp.app.commons.stepper.adapters.AbstractStepperAdapter;
import io.bunifu.erp.app.commons.stepper.interfaces.Step;
import io.bunifu.erp.bunifuschools.R;

public class StepperLayout {

    @BindView(R.id.view_pager)
    MyViewPager viewPager;

    @BindView(R.id.btn_next)
    Button mBtnNext;

    @BindView(R.id.btn_back)
    Button mBtnBack;

    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    private AbstractStepperAdapter stepperAdapter;

    private boolean isForwardNavigation = true;

    private StepperNavigation stepperNavigation = new StepperNavigation() {

        @Override
        public void goToStep(int stepPosition) {
            if (stepPosition >= 0 && stepPosition < stepperAdapter.getCount()) {
                while (getCurrentPosition() != stepPosition) {
                    int currentStep = getCurrentPosition();
                    currentStep = currentStep > stepPosition ? currentStep - 1 : currentStep + 1;
                    setCurrentItem(currentStep);
                }
            }

        }

        @Override
        public void goToNext() {
            if (!isLastStep())
                setCurrentItem(viewPager.getCurrentItem() + 1);
        }

        @Override
        public void goBack() {
            if (!isFirstStep())
                setCurrentItem(viewPager.getCurrentItem() - 1);
        }
    };


    /**
     * @param view Bind view
     */
    public StepperLayout(View view) {
        ButterKnife.bind(this, view);
    }

    /**
     * Set the stepper layout adapter
     *
     * @param stepperAdapter AbstractStepperAdapter
     */
    public void setAdapter(AbstractStepperAdapter stepperAdapter) {

        this.stepperAdapter = stepperAdapter;

        viewPager.setAdapter(stepperAdapter.getPagerAdapter());

        //set navigation listeners
        //go back
        mBtnBack.setOnClickListener(v -> onBack());
        //go to next
        mBtnNext.setOnClickListener(v -> onNext());
        //add a on page changed listener
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                //onCurrentPageSelected(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    /**
     * @param position Selected index
     */
    private void onCurrentPageSelected(int position) {
        //hide back button if position zero
        mBtnBack.setVisibility(position == 0 ? View.INVISIBLE : View.VISIBLE);

        //set next text button
        mBtnNext.setText(position < stepperAdapter.getCount() - 1 ? "Next" : "Complete");

        //set adapter progress
        setProgress(position);

        //on selected
        dispatchOnSelected(position);

    }

    /**
     * Notifies the current fragment that it has been selected
     */
    private void dispatchOnSelected(int position) {
        //notify fragment that its been selected
        Step step = stepperAdapter.findStep(position);
        if (step != null)
            step.onSelected(isForwardNavigation);
    }

    private void setProgress(int position) {
        //update max
        mProgressBar.setMax(stepperAdapter.getCount());
        //set progress = position + 1 (since indexes start from 0 and progress 1)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mProgressBar.setProgress(position + 1, true);
        } else {
            mProgressBar.setProgress(position + 1);
        }
    }

    /**
     * @param position update the current item
     */
    private void setCurrentItem(int position) {
        viewPager.setCurrentItem(position);
        //dispatch on selected
        onCurrentPageSelected(position);
    }

    /**
     * When the next button is clicked, get the current step, notify the user via on next and if it
     * returns true go to the next step.
     */
    private void onNext() {

        //update navigation mode
        setForwardNavigation(true);

        Step step = getCurrentStep();
        if (step != null && step.onNextClicked()) {
            stepperNavigation.goToNext();
        }
    }

    /**
     * When the back button is clicked, get the current step, notify the user via on next and if it
     * returns true go to the next step.
     */
    private void onBack() {

        //update navigation mode
        setForwardNavigation(false);

        Step step = getCurrentStep();
        if (step != null && step.onBackClicked())
            stepperNavigation.goBack();

    }

    /**
     * Set the  navigation to either forward(next clicked) or back
     *
     * @param forwardNavigation NavigationMode
     */
    private void setForwardNavigation(boolean forwardNavigation) {
        this.isForwardNavigation = forwardNavigation;
    }

    private Step getCurrentStep() {
        return stepperAdapter.findStep(getCurrentPosition());
    }

    public int getCurrentPosition() {
        return viewPager.getCurrentItem();
    }

    private boolean isLastStep() {
        return stepperAdapter.getCount() > 0 && getCurrentPosition() == stepperAdapter.getCount() - 1;
    }

    private boolean isFirstStep() {
        return stepperAdapter.getCount() > 0 && getCurrentPosition() == 0;
    }

    public StepperNavigation getStepperNavigation() {
        return stepperNavigation;
    }

    public interface StepperNavigation {

        /**
         * If its not the first and the last step, go to the step at the given index
         *
         * @param stepPosition The step to go to
         */
        void goToStep(int stepPosition);

        /**
         * if its not the last step, go to the next step
         */
        void goToNext();

        /**
         * If it is not the first step, go to the previous step
         */
        void goBack();
    }

}

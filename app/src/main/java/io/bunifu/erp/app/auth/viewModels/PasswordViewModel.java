package io.bunifu.erp.app.auth.viewModels;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;


public class PasswordViewModel extends ViewModel {

    private MutableLiveData<String> password = new MutableLiveData<>();
    private MutableLiveData<String> passwordError = new MutableLiveData<>();
    private MutableLiveData<String> confirmPassword = new MutableLiveData<>();
    private MutableLiveData<String> confirmPasswordError = new MutableLiveData<>();

    public MutableLiveData<String> getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password.setValue(password);
        String error = "";
        if (password.length() > 0 && password.length() < 6) {
            error = "Password should be at-least 6 characters";
        }
        passwordError.setValue(error);
    }

    public MutableLiveData<String> getPasswordError() {
        return passwordError;
    }

    public MutableLiveData<String> getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword.setValue(confirmPassword);
        String error = "";
        if (!confirmPassword.contentEquals(String.valueOf(password.getValue()))) {
            error = "Passwords do not match.";
        }
        passwordError.setValue(error);
        confirmPasswordError.setValue(error);

    }

    public MutableLiveData<String> getConfirmPasswordError() {
        return confirmPasswordError;
    }

    public boolean validate() {
        return String.valueOf(password.getValue()).length() >= 6
                && String.valueOf(confirmPassword.getValue()).contentEquals(String.valueOf(password.getValue()));
    }
}

package io.bunifu.erp.app.attendance.viewModels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

import io.bunifu.erp.app.attendance.data.LessonsModel;
import io.bunifu.erp.app.attendance.data.LessonsRepository;

public class LessonsViewModel extends AndroidViewModel {
    private LiveData<List<LessonsModel>> lessons;
    private LessonsRepository repo;

    public LessonsViewModel(@NonNull Application application) {
        super(application);
        this.repo = new LessonsRepository(application);
        lessons =repo.getAllLessons();
    }

    public LiveData<List<LessonsModel>> getAllLessons() {
      return lessons;
    }

  public void insert(LessonsModel lessonsModel){
        repo.insertAll(lessonsModel);
  }


}

package io.bunifu.erp.app.commons.stepper.adapters;


import android.support.v4.app.FragmentManager;

import java.util.ArrayList;
import java.util.List;

import io.bunifu.erp.app.commons.stepper.interfaces.Step;
import io.bunifu.erp.app.commons.stepper.interfaces.StepAdapter;

public abstract class AbstractStepperAdapter implements StepAdapter {

    private FragmentManager fragmentManager;
    private MyPagerAdapter pagerAdapter;
    private List<Step> steps = new ArrayList<>();

    public AbstractStepperAdapter(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    /**
     * Used to get the step interface implemented by a fragment
     * at the given position.
     *
     * @param position position of the step to get
     * @return Step
     */
    @Override
    public Step findStep(int position) {
        if (steps.size() > position)
            return steps.get(position);
        return null;
    }

    /**
     * Creates an instance of the standard pager adapter from steps
     * @return pagerAdapter
     */
    @Override
    public MyPagerAdapter getPagerAdapter() {
        createPagerAdapter(false);
        return pagerAdapter;
    }

    public MyPagerAdapter refreshAdapter(){
        //recreate adapter
        createPagerAdapter(true);

        return pagerAdapter;
    }


    private void createPagerAdapter(boolean recreate) {
        if (pagerAdapter == null||recreate) {
            for (int i = 0; i < getCount(); i++)
                steps.add(getItem(i));
            pagerAdapter = new MyPagerAdapter(fragmentManager, steps);
        }

    }
}

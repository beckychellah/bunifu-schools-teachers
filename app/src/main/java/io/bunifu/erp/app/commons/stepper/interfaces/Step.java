package io.bunifu.erp.app.commons.stepper.interfaces;

public interface Step {

    /**
     * Notifies a fragment that it has been selected
     */
    void onSelected(boolean isForwardNavigation);

    /**
     * Called when the next button is clicked
     */
    boolean onNextClicked();

    /**
     * Called when the back button is clicked
     */
    boolean onBackClicked();

}

package io.bunifu.erp.database;

import android.app.Application;
import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.migration.Migration;
import android.support.annotation.NonNull;

public class DatabaseBuilder {

    private static final DatabaseBuilder ourInstance = new DatabaseBuilder();

    private static AppDatabase db = null;

    public static DatabaseBuilder getInstance(Application app) {
        if (db == null)
            db = Room.databaseBuilder(
                    app.getApplicationContext(),
                    AppDatabase.class,
                    "bunifu-schools-for-android")
                    .fallbackToDestructiveMigration()
                    .addMigrations(MIGRATION_2_3)
                    .build();

        return ourInstance;
    }

    private static final Migration MIGRATION_2_3 = new Migration(2, 3) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE `notifications` ("
                    +"`id` INTEGER, "
                    +"`studentId` INTEGER, "
                    +"`fcmId` INTEGER, "
                    + "`title` TEXT, "
                    + "`type` TEXT NOT NULL, "
                    + "`body` TEXT, "
                    + "`payload` TEXT, "
                    + "`receivedAt` TEXT, "
                    + "`readAt` TEXT, "
                    +" PRIMARY KEY(`fcmId`))");
        }
    };


    public AppDatabase getDatabase() {
        return db;
    }

}

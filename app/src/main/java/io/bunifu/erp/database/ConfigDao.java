package io.bunifu.erp.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import io.bunifu.erp.service.model.ConfigModel;
import io.reactivex.Flowable;
import io.reactivex.Maybe;

@Dao
public interface ConfigDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(ConfigModel... configModels);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertAsync(ConfigModel configModel);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<ConfigModel> configModels);

    /*@Delete
    void delete(ConfigModel configModel);*/

    @Query("DELETE FROM configs")
    void delete();

    @Query("SELECT * FROM configs WHERE `key` = :key ORDER BY value DESC LIMIT 1")
    ConfigModel getConfigSync(String key);

    @Query("SELECT * FROM configs WHERE `key` = :key ORDER BY value DESC LIMIT 1")
    Maybe<ConfigModel> fetchConfigSync(String key);


    @Query("SELECT * FROM configs WHERE `key` = :key ORDER BY value DESC LIMIT 1")
    Flowable<ConfigModel> getConfigASync(String key);

}

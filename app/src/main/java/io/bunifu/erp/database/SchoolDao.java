package io.bunifu.erp.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import io.bunifu.erp.service.model.SchoolModel;
import io.reactivex.Flowable;

@Dao
public interface SchoolDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(SchoolModel... schoolModels);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<SchoolModel> schoolModels);

    @Query("DELETE FROM schools")
    void delete();

    @Query("SELECT * FROM schools WHERE `id` = :schoolId LIMIT 1")
    SchoolModel getSchoolSync(int schoolId);

    @Query("SELECT * FROM schools WHERE `id` = :schoolId LIMIT 1")
    Flowable<SchoolModel> getSchoolASync(int schoolId);


}

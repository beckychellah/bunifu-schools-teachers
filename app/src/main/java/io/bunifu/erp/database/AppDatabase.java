package io.bunifu.erp.database;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import io.bunifu.erp.app.attendance.data.LessonsDao;
import io.bunifu.erp.app.attendance.data.LessonsModel;
import io.bunifu.erp.service.model.ConfigModel;
import io.bunifu.erp.service.model.SchoolModel;
import io.bunifu.erp.service.model.StudentModel;


@Database(entities = {ConfigModel.class, StudentModel.class,
        SchoolModel.class, LessonsModel.class}, version = 5, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public abstract ConfigDao getConfigDao();

    public abstract StudentDao getStudentDao();

    public abstract SchoolDao getSchoolDao();

    public abstract LessonsDao getLessonsDao();

}

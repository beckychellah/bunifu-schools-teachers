package io.bunifu.erp.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import io.bunifu.erp.service.model.StudentModel;
import io.reactivex.Flowable;
import io.reactivex.Maybe;

@Dao
public interface StudentDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(StudentModel... studentModels);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<StudentModel> studentModels);

    @Query("DELETE FROM students")
    void delete();

    @Query("SELECT * FROM students WHERE `id` = :studentId LIMIT 1")
    StudentModel getStudentSync(int studentId);

    @Query("SELECT * FROM students WHERE `id` = :studentId LIMIT 1")
    Flowable<StudentModel> getStudentASync(int studentId);

    @Query("SELECT * FROM students")
    Flowable<List<StudentModel>> fetchAllStudents();

    @Query("SELECT * FROM students WHERE id <> :studentId ")
    Flowable<List<StudentModel>> fetchAllStudentsExcept(int studentId);

    @Query("SELECT * FROM students WHERE `id` = :studentId LIMIT 1")
    Maybe<StudentModel> getById(int studentId);

    @Query("SELECT * FROM students WHERE `schoolId` = :schoolId LIMIT 1")
    Maybe<StudentModel> getStudentBySchoolId(int schoolId);
}

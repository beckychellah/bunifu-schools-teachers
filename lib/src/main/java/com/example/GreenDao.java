package com.example;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class GreenDao {
    private static final String PROJECT_DIR = System.getProperty("user.dir");

    public static void main(String[] args) {
        Schema schema = new Schema(1, "io.bunifu.erp.models.utils");
        schema.enableKeepSectionsByDefault();

        addConfig(schema);
        try {
            new DaoGenerator().generateAll(schema, PROJECT_DIR + "/app/src/main/java");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Entity addConfig(final Schema schema) {

        Entity config = schema.addEntity("Config");
        config.addIdProperty().autoincrement();
        config.addLongProperty("config_id");
        config.addStringProperty("key");
        config.addStringProperty("value");
        return config;

    }
}
